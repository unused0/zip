#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>

#define MEMSZ 65536

static bool rd[MEMSZ];
static bool wr[MEMSZ];
static bool ex[MEMSZ];
static char buf[512];

#define MEM_RD 1 /* memory read */
#define MEM_WR 2 /* memory write */
#define MEM_EX 4 /* executed opcode */
#define MEM_OP 8 /* executed operand */
static uint8_t coverage[0x10000];

int main (int argc, char * argv[]) {
  int cvfd = open ("coverage.dat", O_RDONLY);
  if (! cvfd) {
    fprintf (stderr, "can't open coverage.dat\n");
    exit (2);
  }

  ssize_t nr = read (cvfd, coverage, sizeof (coverage));
  if (nr != sizeof (coverage)) {
    fprintf (stderr, "can't read coverage.dat\n");
    exit (2);
  }

  FILE * list = fopen ("zip.lst", "r");
  if (! list) {
    fprintf (stderr, "can't open zip.lst\n");
    exit (2);
  }

  char flags[5];
  flags[4] = 0;
  buf[0] = 0;
  while (! feof (list)) {
    flags[0] = ' ';
    flags[1] = ' ';
    flags[2] = ' ';
    flags[3] = ' ';
    fgets (buf, sizeof (buf), list);
    //printf (">%c%c%c%c%c %u %s\n", buf[6], buf[7], buf[8], buf[9], buf[12], isxdigit (buf[6]) && isxdigit (buf[7]) && isxdigit (buf[8]) && isxdigit (buf[9]) && isxdigit (buf[12]), buf);
    if (isxdigit (buf[6]) && isxdigit (buf[7]) && isxdigit (buf[8]) && isxdigit (buf[9]) && isxdigit (buf[12])) {
      char * endptr;
      char * strtptr = buf + 6;
      long addr = strtol (strtptr, & endptr, 16);
      if (endptr - strtptr == 4) {
        //printf (">> %x %x %x\n", n, addr, coverage[addr]);
        if (coverage[addr] & MEM_RD)
          flags[0] = 'R';
        if (coverage[addr] & MEM_WR)
          flags[1] = 'W';
        if (coverage[addr] & MEM_EX)
          flags[2] = 'X';
        if (coverage[addr] & MEM_OP)
          flags[3] = 'O';
      } else {
        flags[0] = '-';
        flags[1] = '-';
        flags[2] = '-';
        flags[3] = '-';
      }
    }

    printf ("%4s %s", flags, buf);
  }
  return 0;
}



  
 
