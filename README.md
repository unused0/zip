# An implementation of ZIP from "Threaded Interpretive Languages" by R. G. Loeliger.

## Manifest

- zip.asm: Source code

- list.sh: A source processing script invoked by the Makefile; it threads the dictionay entries into a linked list.

- Makefile:  Build zip.bin from zip.asm

- TIL_Errate.txt: Errors noticed in the book.

## Dependencies

The Makefile uses the z80asm assembler.

The "make check" target requires the "expect" program.

All testing and development has been done with the OpenSIMH AltairZ80 simulator.

The simh configuration scripts may have OpenSIMH dependencies.

## Building

"make" will build:

- zip.bin: Runs without OS support; loads and starts at address 0.
Assumes the default OpenSIMH AltairZ80 I/O configuration.

- mg.bin: (Untested; under development) Build targeted for 
"V2 - Z80 Master/Slave CPU Board" from [s100computers.com](s100computers.com).

- cpm2.hex: Built targeted for a Z80 CP/M V2.2 system.

In the OpenSIMH distribution, the file doc/altairz80_doc.docx contains links
to CP/M builds that will run on the SIMH simulator. Development of the
cpm2.hex target was done by downloading the CP/M builds and doing:


    cp <path>/zip.hex ZIP.HEX
    altairz80 cpm2
    A> R ZIP.HEX
    A> LOAD ZIP
    A> ZIP

"make check" runs a ZIP testing script.

"make test" and "make coverage" assume a heaivly modified AltairZ80 simulator.


## Notes

Development diary at
https://docs.google.com/document/d/104p9hy37mOBVOLbOs878gOJTrlYLlPYHpfQqomZgRjg/edit?usp=sharing

## Status:

- All words up to chapter 6 ("Words, Words, and More Words") transcribed.
- 99% Coverage tests

## Bugs:

SIMH is not setting the console output to raw mode, so the ZIP output is
double spaced.

In the coverage test, the "long line" test is putting a "!" in the buffer for 
some reason.

Something is wrong with "\_abort" and/or "\_patch"; they are not correctly
fixing the vocabularies for the case of an error during compilation. It is not
clear if this is an implementation bug or a design bug.


## Running ZIP

    altairz80 zip.ini

This will start ZIP; type ^E to exit.

ZIP will print

    ZIP

Type:

    1 2 3 . . .

followed by the enter key. ZIP will respond:

    3 2 1  OK


## Implementation notes

The book references "PAD" but does not define it. I have have implemented what I think
it does; but no promises.

CLEAR is implemented wih a form-feed character ('\f', 0x0C); this is probably
not correct for many installations.

The STS and inpit buffer areas must be aligned to 256 byte
boundaries. Current;y, they are in the middle of the executable
and need to be moved to the end of ram. But that address varys
across systems, and need to be made a configurable parameter, or
the startup code needs to position them dynamically.

The definition of "APART" uses the keyword "OR", but the text
defines the keyword as "IOR". I find "OR" more natural, but
"IOR" seems more in keeping with ZIP's usage.

I worked out the reason that my vocabulary test was breaking the
check test; it is neccesary to "CORE DEFINITIONS" before forgeting
a "foo DEFINITIONS".


## Coverage testing

In patch, the "jr nc,skip" fall-through case is not tested; it
word require aligning the dictionary entry acress a page boundary.

This seems wrong:

     DECIMAL 2048 256 / .
     239  OK

I am unable to come up with the correct plumbing to test the
"'" (tick) handling of searching current.

Wierd. CJOIN and CPLIT are asymmetric.

Coverage of D/MOD and D* is poor.

Page boundary crossings not covered in _patch, s_else, and s_loop.


## Observations about ZIP.

FORGET is deficient; if there is a DEFINITION to be forgotten, it is 
necessary to do a "CORE DEFINITIONS" before the FORGET.

Something is wrong with "\_abort" and/or "\_patch"; they are not correctly
fixing the vocabularies for the case of an error during compilation. It is not
clear if this is an implementation bug or a design bug.

ZIP vocabularies are deficient; specifically, ?SEARCH searches CONTEXT and COMPILE
but not current.

On PDP-11 FIG-FORTH:

    : MARK ; OK
    VOCABULARY FOO OK
    : BAR ; OK
    FOO DEFINITIONS OK
    : BAZ ; OK
    : FUBAR BAZ BAR ; OK

With my TIL implementation:

    VOCABULARY FOO OK
    : BAR ; OK
    FOO DEFINITIONS OK
    : BAZ ; OK
    : FUBAR BAZ BAR ;
    BAR ?

Also:

PDP-11 FIG-Forth:

    FIG-FORTH  V 1.3.3 
    VOCABULARY FOO  OK
    : BAR ;  OK
    FOO DEFINITIONS  OK
    : BAZ ;  OK
    ' BAR . 7416  OK

ZIP:

    VOCABULARY FOO  OK
    : BAR ;  OK
    FOO DEFINITIONS  OK
    : BAZ ;  OK
    ' BAR . 
    BAR ?
    CORE  OK
    ' BAR .  154C  OK


## TO DO

Add disk support.

Transcripe editor and assembler code.

