#! /usr/bin/awk  -f 

# ./annotate.sh zip < zip.debug > zip.debug.annotated

BEGIN {
  base=ARGV[1]
  ARGV[1]=""
  file=base".lst"
  while ((getline line <file) > 0) {
    if (substr (line, 1, 13) == "Symbol Table:") {
      break
    }
  }
  while ((getline line <file) > 0) {
    split (line, sym)
    if (strlen (sym[1]) > 0) {
      if (substr (sym[2], 1, 1) != "=") {
        addr = substr ("0000", 1, 4 - length (sym[2])) sym[2]
        syms[addr] = sym[1]
        if (sym[1] == "_run") {
          run_addr = addr
          print "# run address " run_addr
        }
        if (sym[1] == "_go") {
         go_addr = addr
        print "# go address " go_addr
        }
        if (sym[1] == "rstack") {
          rstk_addr = strtonum ("0x" addr)
          print "# rstk address " rstk_addr
        }
        spaces = "                                                        "
      }
    }
  }
  close (file)

  lfile=base".lst"
  while ((getline line <lfile) > 0) {
    addr=toupper (substr (line,7,4))
    if (strlen (listing[addr])) {
      listing[addr] = listing[addr] "\n" line
    } else {
      listing[addr] = line
    }
  }
}

/^CPU:/ {
   print "\n"
   pc =  substr ($14, 2)
   print listing[pc]
#   cmd = "grep -i ^" pc " "base".list"
#   while ( ( cmd | getline res ) > 0 ) {
#     print res
#   }
#   close (cmd)
   if (pc == run_addr) {
     hl =  substr ($10, 2)
     depth = rstk_addr - strtonum ("0x" ix)
#print depth, rstk_addr, ix
#     print "inner  " hl "   " substr (spaces, 1, depth)  "  "  syms[tolower(hl)]
   }
   if (pc == go_addr) {
     print "inner  " hl "   " substr (spaces, 1, depth)  "  "  syms[toupper(hl)]
   }
}
/IX=/ {
   ix = substr ($5,4)
}

#/^.... ..                  go:/ {      print "inner  " hl "   " substr (spaces, 1, depth)  "  "  syms[tolower(hl)] }
{ print $0 }

