
ifndef	extensions
extensions	equ	0
endif

ifndef	mg
mg	equ	0
endif

ifndef	raw
raw	equ	0
endif

ifndef	cpm2
cpm2	equ	0
endif

ifndef	trsdos
trsdos	equ	0
endif

ifndef	bdosdisk
bdosdisk	equ	0
endif

ifndef	biosdisk
biosdisk	equ	0
endif

; Registers
;  AF
;  BC  Instruction register
;  DE  Word address register, scratch
;  HL  Scratch
;  IX  Return stack ptr
;  SP  Data stack ptr
;  AF' Scratch
;  BC' Scratch
;  DE' Scratch
;  HL' Scratch

nxt	macro
	jp	(iy)
	endm

__link	defl	0
__link__	macro
	dw	__link
__link	defl	$-6
	endm

__ilink	defl	0
__ilink__	macro
	dw	__ilink
__ilink	defl	$-6
	endm

LF	equ	10	; ASCII linefeed
CR	equ	13	; ASCII carriage return
NAK	equ	21	; ASCII control U (NAK)
BS	equ	8	; ASCII backspace

;	__raw__
;	__mg__
;	__cpm2__
;	__trsdos__
;	__biosdisk__
;	__bdosdisk__
;	__ext__

; Memory layout

; Data Stack
; Return Stack
; System Variables
; 128 byte line buffer + terminator

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Notation
;;;

; In FIG-FORTH, the runtime code for compiling words was usually denoted
; as "(word)"'; eg "ELSE" generated runtime code that called "(ELSE)".
; In the assembly langage source, the words would be labeled "else" and
; "pelse". (The leading "p" denoting "parenthesis".
;
; In Threaded Interpretive Languages, the words would be "ELSE" and "*ELSE";
; the assembly labels are not defined. The assembler I am using (z80asm)
; is comfortable with longer names, so I am using the convention of a
; "s_" prefix for "*".
;
; Code called directly (subroutines for example) are denoted with a leading
; underscore.
;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Configuration
;;;

if mg
port0:	equ	0x00	; sense
;keyrdy:	equ	1	; test bit 1 to check if keypress
keyrdy:	equ	2	; rdy mask
port1:	equ	0x01	; data
endif

if raw
port0:	equ	0x10	; sense
;keyrdy:	equ	0	; test bit 0 to check if keypress
keyrdy:	equ	1	; rdy mask
port1:	equ	0x11	; data
endif

if cpm2
bdoss:	equ	0x0005
conin:	equ	1
conout:	equ	2
constatus:	equ	11
conraw:	equ	6
topram:	equ	7	; MSB of BDOS entry point
endif

dstksz:	equ	512	; data stack size in bytes
rstksz:	equ	512	; return stack size in bytes

; Dictionary
;
;   7             Header for EXECUTE
;   E
;   X
;   E
;   link
;   body          word address of EXECUTE
;   ...


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Memory layout
;;;
;;;   raw, mg
;;;
;;;       loads at 0
;;;       
;;;          0   _start:   start/restart code
;;;          n   sys_block
;;;              messages
;;;              stack
;;;              return stack
;;;              line buffer
;;;              dictionary
;;;              new code
;;;
;;;   cpm2 (not yet implemented)
;;;
;;;      loads at 0x100
;;;        100  _start
;;;             messages
;;;             dictionary
;;;             new code
;;;
;;;             stack
;;;             return stack
;;;             sys_block
;;; end of ram  line buffer
;;;
;;;
;;;   TRS DOS
;;;
;;;    loads at 0x4000
;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Entry
;;


	org	0

if cpm2
	org	0x100
endif
if trsdos
	org	0x4000
endif

;;;
;;; START
;;;

; TIL, pg 87 (102)

_start:
if bdosdisk
;	ld	a,topram	; get MS byte of BDOS entry point
;	dec	a		; back off one page
;	ld	l,0xff		; set LSB of final address
;	ld	h,a		; HL = xxFF
;	ld	(eom),hl
CLD:
	ld	hl,(bdoss+1)
	ld	l,0		; (HL)<--FBASE
	ld	(_limit),hl	; set LIMIT
	ld	de,bufsiz	; (DE)<--total disc buffer size
	or	a		; clr carry
	sbc	hl,de		; (HL)<--addr. of first disk buffer
	ld	(_first),hl	; set FIRST
	ld	(_use),hl	; set USE
	ld	(_prev),hl	; set PREV
	ld	(buf1),hl	;
endif
	ld	de, rstmsg	; restart message address to WA
	ld	a,(s_base)	; get system base
	and	a		; test for 0
	jr	nz,_abort		; if it is 0, it is a start
	ld	a,16		; else get hex base
	ld	(s_base),a	; and store it at base
	ld	de,srtmsg		; start message address to WA
_abort:	ld	sp,stack		; set system data stack
	push	de		; push message address
	ld	hl,0
	ld	(s_mode),hl		; set mode=0, state=0
	ld	iy,_next		; set inner interpreter next address to iy
	ld	ix,rstack		; set return stack
	ld	hl,0x8080		; get two terminators
	ld	(lbterm),hl	; store to end of line buffer
	ld	hl,outer
	jp	_run

;if cpm2
;eom:	dw	0		; calculated last word in memory
;endif

if bdosdisk
buf1:	dw	0		; address of first disk buffer
endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Inner interpreter
;;;

; TIL, pg 36 (51)

s_semi:	dw	$+2
	ld	c,(ix+0)
	inc	ix
	ld	b,(ix+0)
	inc	ix

; WA <= @IR
; IR += 2
_next:	ld	a,(bc)	; low byte of *IR
	ld	l,a	; into L
	inc	bc	; IR ++
	ld	a,(bc)	; high byte of *IR
	ld	h,a	; into H
	inc	bc	; IR ++

; CA <= @WA
; WA += 2
; PC <= CA

_run:	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	hl
	ex	de,hl
; the 'go' label is for the debugging tools
_go:	jp	(hl)

; push IR to return stack
; IR <= WA
; next

_s_colon:
	dec	ix	; Push IR to return stack
	ld	(ix+0),b
	dec	ix
	ld	(ix+0),c
	ld	c,e	; IR <= WA
	ld	b,d
	nxt

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Subroutines
;;;


;;;
;;; $PATCH
;;;

; TIL pp 98-99 (113-114)

; The routine $PATCH is a machine language routine that is used to patch
; system variables in the event a system-detected error occurs during the com-
; pile mode. Any system-detected error that could occur during the compile
; mode should enter START/RESTART via $PATCH. System-detected errors
; that can occur only in the execute mode may jump unconditionally to
; START/RESTART.
; $PATCH resets the dictionary pointer DP and the CURRENT vocabulary
; link to the values that existed prior to the start of the aborted compile mode
; definition. If the MODE of the system is the compile mode on entry, $PATCH
; resets DP to the address that is the header address of the latest keyword in the
; CURRENT vocabulary. The link address in this header is then stored as the
; pointer to the latest entry in the CURRENT vocabulary. This delinks the par¬
; tially entered keyword from the system and re-establishes the dictionary free
; space to its previous value. A Z80 assembly code listing for this routine is
; given as listing 5.8.

_patch:	ld	a,(s_mode)	; get mode variable
	and	a		; is it zero (execute)?
	jp	z,_abort	; if so, restart
	push	de		; else save message address
	ld	hl,(s_current)	; get vocabulary address
	ld	e,(hl)		; it points to the latest
	inc	hl		;   entry, which was aborted
	ld	d,(hl)		; this is where DP should
	ex	de,hl		;    point
	ld	(s_dp),hl	; restore DP
	ld	a,5		; bump ptr to the
	add	a,l		;   link address of the aborted word
	ld	l,a		;   by 5
	jr	nc,patch_skip	;
	inc	h		;
patch_skip:
	ld	a,(hl)		; move link address to the
	ld	(de),a		;   current vocabulary as
	dec	hl		;   the pointer to its
	dec	de		;   latest entry
	ld	a,(hl)		;
	ld	(de),a		;
	pop	de		; restore message address
	jp	_abort		; and exit to restart








;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; SYS storage
;;;

; XXX XXX XXX XXX
; _sys is optimized by assuming that the buffer is at a
; 256 byte address boundary; the 'ds' below wastes memory by
; movbing up. A better solution is to put the buffer at the end
; of memory

;;; advance to page boundary

	ds	(($ + 0x00ff) & 0xff00) - $, 0

sys_block:

s_mode:		dw	0
s_base:		dw	0
s_lbp:		dw	0
s_state:	dw	0
s_dp:		dw	dp0
s_compiler:	dw	lastilink
s_context:	dw	vcore
s_current:	dw	vcore
if bdosdisk
s_offset:	dw	0
endif









;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Messages
;;;



srtmsg:	db	srtmsgx-$-1,CR,LF,"ZIP V0.1.0",CR,LF
srtmsgx:

rstmsg:	db	rstmsgx-$-1,CR,LF,"RESTART",CR,LF
rstmsgx:

ok:	db	okx-$-1," OK",CR,LF
okx:

msgq:	db	msgqx-$-1," ?"
msgqx:

stkmsg:	db	stkmsgx-$,CR,LF,"STACK UNDERFLOW",CR,LF
stkmsgx:








;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Stacks
;;;

	ds	dstksz
stack:
	ds	rstksz
rstack:








;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Line buffer
;;;

; XXX XXX XXX XXX
; The code is optimized by assuming that the buffer is at a
; 256 byte address boundary; the 'ds' below wastes memory by
; movbing up. A better solution is to put the buffer at the end
; of memory

;;; advance to page boundary

	ds	(($ + 0x00ff) & 0xff00) - $, 0

lblen:	equ	126
lbadd:	ds	lblen
lbterm:	ds	2








;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Dictionary
;

;;;
;;; !
;;;

; TIL pg 104 (119)

; Class:	Memory Reference
; Function:	Stores second stack entry at the address at the top stack en¬
;		try, removing both entries.
; Input/Output:	Two stack entries/None
; Usage:	Storage of word length data in programmable memory.
; Z80 Code:

	db	1,"!  "
	__link__
store:	dw	$+2
	pop	hl	; get address
	pop	de	; get data
	ld	(hl),e	; store byte
	inc	hl	; bump address
	ld	(hl),d	; store byte
	nxt

; Bytes:	19








;;;
;;; #
;;;

; TIL pg 104 (119)

; Class:	I/O
; Function:	Pops the top stack entry, computes the quotient and re¬
;		mainder relative to the system number base, converts the
;		remainder to an ASCII character (0 thru 9, A thru Z),
;		pushes the character, then pushes the quotient.
; Input/Output:	One stack entry/Two stack entries.
; Usage:	Does one conversion in the process of generating format¬
;		ted display outputs.
; Code:

	db	1,"#  "
	__link__
sharp:	dw	_s_colon
	dw	zero	; 24 bit number extension
	dw	base	; number base address
	dw	cat	; number base
	dw	ddmod	; remainder then quotient
	dw	ascii	; remainder to ASCII character
	dw	swap	; remainder to number string
	dw	s_semi

; Bytes:	22
; Formal Definition:
;		: # 0 BASE C@ D/MOD ASCII SWAP ;








;;;
;;; #>
;;;

; TIL pg 105 (120)

; Class:	I/O
; Function:	Pops the the sign byte left on the return stack by <#,
;		discards it and then displays the string on the stack using
;		the DISPLAY format convention.
; Input/Output:	One return stack entry and a variable length stack
;		string/None.
; Usage:	Display formatted strings built onto the stack
; Z80 Code:

	db	2,"#> "
	__link__
sharpend:
	dw	$+2
	inc	ix	; drop return
	jp	_display	; go echo string









;;;
;;; #S
;;;

; TIL pg 105 (120)

; Class:	I/O
; Function:	Converts the top stack entry to a sequence of ASCII
;		characters equivalent to the entry given the current system
;		number base. Sequentially pushes the number characters
;		with the most significant character to the top stack entry.
; Input/Output:	One stack entry/One to sixteen stack entries.
; Usage:	Converting numbers to a display string
; Code:

	db	2,"#S "
	__link__
sharps:	dw	_s_colon
sharps1:
	dw	sharp	; convert one character
	dw	dup	; dup quotient
	dw	zeq	; is it zero?
	dw	s_end	; if not, loop
	db	sharps1-$
	dw	drop	; drop zero quotient
	dw	s_semi

; Bytes:	12
; Notes:	Always does at least one conversion producing an ASCII 0
;		if the top entry was 0.
; Formal Definition:
;		: #S BEGIN # DUP 0 = END DROP ;








;;;
;;; $CRLF
;;;

; TIL pg 105-106 (120-121)

; Class:	Subroutine
; Function:	Issue a carriage return-line feed sequence to the display to
;		scroll the display if required, clear the next display line and
;		leave the cursor at the left end of the blank line.
; Input/Output:	None/None.
; Usage:	Formatting the display.

_crlf:	ld	a,CR	; get a CR
	call	_echo	; display it
	ld	a,LF	; get a lf
	call	_echo	; display it
	ret

; Bytes:	11








;;;
;;; $ECHO
;;;

; TIL pg 106 (121)

; Class:	Subroutine
; Function:	Interfaces the system to the display
; Input/Outut:	None/None.
; Usage:	Available only to the system.
; Code:		Not applicable.

if cpm2
_echo:	push	de		; save DE
	push	hl		; save HL
	push	bc		; save BC
	ld	e,a
	ld	c,conout
	call	bdoss
	pop	bc		; restore BC
	pop	hl		; restore HL
	pop	de		; restore DE
	ret
endif

if trsdos
_echo:	ret
endif

if raw
_echo:
	ex	af,af'	; save AF
_echo1:	in	a,(port0)	; get print ready
	;and	0x02
	bit	1,a
	jr	z,_echo1
	ex	af,af'	; restore AF
	out	(port1),a
	ret
endif

if mg
_echo:
	ex	af,af'	; save AF
_echo1:	in	a,(port0)	; get print ready
	;and	0x02
	bit	1,a
	jr	z,_echo1
	ex	af,af'	; restore AF
	out	(port1),a
	ret
endif

; Notes:	Usually called via a transfer vector. $ECHO must interface
;		to existing system software or may be specifically written
;		to field the display function for the TIL.








;;;
;;; $ISIGN
;;;

; TIL pg 106-107 (121-122)

; Class:	Subroutine
; Function:	Computes and saves the sign of an arithmetic result and
;		converts negative integers to positive integers.
; Input/Output:	None/None.
; Usage:	Available only to the system. See Section 5.5.
; Z80 Code:

_isign:	ld	a,d		; sign of 1st
	xor	b		; xor sign of second
	ex	af,af'		; result sign to af'
	ld	a,d		; sign of 1st
	and	a		; test sign, CY = 0
	jp	p,_isign_tst2	; if +, it's okay
	ld	hl,0		; else get 0
	sbc	hl,de		; make first positive
	ex	de,hl
_isign_tst2:
	ld	h,b		; move 2nd high byte
	ld	l,c		; move 2nd low byte
	ld	a,b		; sign of second
	and	a		; test sign, CY = 0
	ret	p		; if +, return
	ld	hl,0		; else get zero
	sbc	hl,bc		; make 2nd positive
	ret			; return

; Bytes:	25
; Notes:	Numbers in DE and BC on entrance and DE and HL on ex¬
;		it. Result sign in AF' on exit








;;;
;;; $KEY
;;;

; TIL pg 107 (122)

; Class:	Subroutine
; Function:	Interfaces the keyboard to the system.
; Input/Output:	None/None.
; Usage:	Available only to the system.
; Code:		Not applicable

if cpm2
_key:
	push	de
	push	hl
	push	bc
;	ld	c,conin
_key1:	ld	c,conraw
	ld	e,0xff	; Return a character without echoing if one is waiting; zero if none is available.
	call	bdoss
	and	a
	jr	z,_key1
	pop	bc
	pop	hl
	pop	de
	ret
endif

if trsdos
_key:	ld	a,0
	ret
endif

if raw | mg
_key:	in	a,(port0)
;	bit	keyrdy,a
	and	keyrdy
	jr	z,_key
	in	a,(port1)
	ret
endif

; Notes:	Usually called via a transfer vector. $KEY must interface to
;		existing system software or may be specifically written to
;		field keyboard data for the TIL.








;;;
;;; $OSIGN
;;;

; TIL pg 107 (122)

; Class:	Subroutine
; Function:	Negates a positive integer arithmetic result if the result sign
;		bit is 1 set.
; Input/Output:	None/None.
; Usage:	Available only to the system. See Section 5.5.
; Z80 Code

_osign:
	ex	af,af'	; retreive sign flags
	ret	p	; if +, sign is okay
	ex	de,hl	; else result to DE
	ld	hl,0	; zero hl
	sbc	hl,de	; minus result to HL
	ret

; Bytes:	9
; Notes:	Result in HL on entrance and exit. Result sign bit in AF' on
;		entrance.








;;;
;;; $UD*
;;;

; TIL pg 107 (122)

; Class:	Subroutine
; Function:	Multiplies a 16-bit unsigned integer by an 8-bit unsigned
;		integer and returns a 24-bit product.
; Input/Output:	None/None.
; Usage:	Available only to the system. See Section 5.5.
; Z80 Code:

_udm:	ld	a,l		; multiplicand to A
	ld	bc, 0x0800	; count = 8, dummy = 0
	ld	h,c		; zero high result
	ld	l,c		; zero low result
_udm_loop:
	add	hl,hl		; shift result and
	adc	a,a		; multiplicand left 1
	jr	nc,_udm_kadd	; if CY == 0, skip add
	add	hl,de		; add multiplier
	adc	a,a		; propogate carry
_udm_kadd:
	djnz	_udm_loop	; loop 8 times
	ld	c,a		; + high 8 bits in C
	ret			; low 16 in HL

; Bytes:	16
; Notes:	On entrance, L contains an 8-bit multiplicand and DE con¬
;		tains a 16-bit multiplier. On exit BC contains the most
;		significant 8 bits and HL the 16 least significant bits. No
;		test is made to verify a valid 8-bit number in L on entrance.








;;;
;;; $US*
;;;

; TIL pg 107 (122)

; Class:	Subroutine
; Function:	Multiplies an 8-bit unsigned integer by an 8-bit unsigned
;		integer and returns a 16-bit product.
; Input/Output:	None/None.
; Usage:	Available only to the system. See Section 5.5
; Z80 Code:

_usmul:
	ld	h,l		; multiplicand to H
	ld	l,0		; zero result low
	ld	d,l		; multiplier high = 0
	ld	b,8		; set multiple count
_usmul_loop:
	add	hl,hl		; shift result and multiplicand
	jr	nc,_usmul_skad	; if CY == 0, skip add
	add	hl,de		; add multiplier
_usmul_skad:
	djnz	_usmul_loop	; loop 8 times
	ret			; result in HL

; Bytes:	13
; Notes:	On entrance, L and E contain 8-bit unsigned integers and H
;		and D are presumed 0 (assumes valid 8-bit numbers). On
;		exit, HL contains the 16-bit product.








;;;
;;; $UD/
;;;

; TIL pg 107-108 (122-123)

; Class:	Subroutine
; Function:	Divides a positive 24-bit integer by a positive 8-bit integer
;		and returns a positive 8-bit remainder and 16-bit quotient
; Input/Output:	None/None.
; Usage:	Available only to the system. See Section 5.5
; Z80 Code:

; B  divide count
; HL dividend low 16
; D  divident high 8
;  E divisor

_uddiv:	ld	b, 16		; divide count 16
_udduv_loop:
	add	hl,hl		; shift low 16
	ld	a,d		; get high 8
	adc	a,d		; shift high 8
	ld	d,a		; restore high
	sub	e		; subtract divisor
	jp	m,_uddiv_skip	; too much, it;s ok
	inc	l		; set result low bit = 1
	ld	d,a		; decrease dividend
_uddiv_skip:
	djnz	_udduv_loop	; loop 16 times
	ld	c,d		; remainder to C
	ret			; quotient in HL

; Bytes:	16
; Notes:	On entrance, D,HL contains a 24-bit positive dividend and
;		E contains an 8-bit positive divisor. On exit, C contains an
;		8-bit remainder and HL contains a 16-bit quotient. No test
;		is made to verify a correct 16-bit quotient.








;;;
;;; $US/
;;;

; TIL pg 109 (124)

; Class:		Subroutine
; Function:	Divides a positive 16-bit dividend by a positive 8-bit
;		divisor and returns a positive 8-bit remainder and 8-bit
;		quotient.
; Input/Output:	None/None.
; Usage:	Available only to the system. See Section 5.5
; Z80 Code:

_usdiv:	ld	b,8		; divide count = 8
_usdiv_loop:
	add	hl,hl		; shift dividend
	ld	a,h		; get high byte
	sub	e		; subtract divisor
	jp	m,_usdiv_skip	; too much, it's ok
	inc	l		; set result low bit = 1
	ld	h,a		; decrease divident
_usdiv_skip:
	djnz	_usdiv_loop	; loop 8 times
	ld	c,h		; remainder in C
	ld	h,b		; result high = 0
	ret			; result in HL

; Bytes:	15
; Notes:	On entrance, HL contains a positive 16-bit dividend and E
;		contains a positive 8-bit divisor. On exit, C contains an
;		8-bit remainder and L contains an 8-bit quotient. No test is
;		made to verify a correct 8-bit quotient.








;;;
;;; '
;;;

; TIL pg 110 (125)

; Class:	System
; Function:	Scans the token following the ' (tick) in the input buffer
;		and searches the CURRENT and CONTEXT vocabularies
;		for the keyword corresponding to the token. Returns the
;		word address of the keyboard as the top stack entry if it is
;		located. If not found, the token is echoed to the operator
;		and followed by a "?".
; Input/Output:	None/One stack entry or none.
; Usage:	Operator location of keywords
; Code:

	db	1,"'  "
	__link__
tick:	dw	_s_colon
	dw	aspace	; get the seperator
	dw	token	; scan the next token
	dw	context	; context address
	dw	at	; contains vocabulary address
	dw	at	; contains the latest entry
	dw	search	; search the vocabulary
	dw	s_if	; if false, found; otherwise
	db	tick1-$
	dw	entry	; get latest current
	dw	search	; search current
	dw	s_if	; if false, found; otherwise
	db	tick1-$
	dw	question	; echo token and ?
tick1:	dw	s_semi

; Bytes:	34
; Formal Definition:
;	: '
;	    ASPACE TOKEN
;	    CONTEXT @ @ SEARCH IF
;	        ENTRY SEARCH IF
;	            QUESTION
;	        THEN
;	     THEN ;








;;;
;;; *
;;;

; TIL pg 110 (125)

; Class:	Arithmetic
; Function:	Does a signed multiply of the second stack word by the
;		low-order byte of the top stack entry and replaces both en¬
;		tries by the 16-bit (word) product.
; Input/Output:	Two stack entries/One stack entry.
; Usage:	Signed integer arithmetic.
; Z80 Code:

	db	1,"*  "
	__link__
mul:	dw	$+2
	exx		; save IR
	pop	bc	; get first
	pop	de	; get second
	call	_isign	; field input signs
	call	_udm	; multiply 16x8
	call	_osign	; Justify result
	push	hl	; Result to stack
	exx		; restore IR
	nxt

; Bytes:24
; Notes:	Does not test the top stack entry to insure it is a valid 8-bit
;		number. No test is made to insure a valid 16-bit product.








;;;
;;; *#
;;;
;
;  TIL pg 111 (126)

; Class:		Literal Handler (Headerless)
; Function:	Pushes to the stack the word whose address is in the in¬
;		struction register and increments the instruction register
;		twice (past the word literal).
; Input/Output:	None/One stack entry.
; Usage:		Available only to the system.
; Z80 Code:

	; headerless
literal:
	dw	$+2
	ld	a,(bc)	; get byte at IR
	ld	e,a	; move it to DE
	inc	bc	; bump IR
	ld	a,(bc)	; get byte at IR
	ld	d,a	; move it to DE
	inc	bc	; bump IR
	push	de	; push literal
	nxt

; Bytes:		11








;;;
;;; *+LOOP
;;;

; TIL pg 111 (126)

; Class:	Program Control Directive (Headerless)
; Function:	Gets the return stack pointer, pops the index byte from the
;		stack, and then transfers to the *LOOP code to mechanize
;		a non-unity indexed loop.
; Input/Output:	One stack entry/None.
; Usage:	Available only to the system.
; Z80 Code:

	; headerless
s_plusloop:
	dw	$+2
	push	ix	; get return stack
	pop	hl	; to the registers
	pop	de	; get inc byte
	ld	a,e	; to the A register
	jp	_s_loop	; jump to *LOOP code

; Bytes:	10
; Notes:	* -I-LOOP has a code address but not a return address.
;		Increments must be in the set — 128<I<127.








;;;
;;; */
;;;

; TIL pg 111-112 (126-127)

; Class:	Arithmetic
; Function:	Does a signed multiply of the third stack word by the low-
;		order byte of the second stack word and a signed divide of
;		the 24-bit product by the low-order byte of the top stack
;		entry. Replaces the three entries with the 16-bit quotient.
; Input/Output:	Three stack entries/One stack entry.
; Usage:	Signed integer arithmetic.
; Z80 Code:

	db	2,"*/ "
	__link__
muldiv:
	dw	$+2
	ld	iy,_muldiv_return_to	; change next return
	jp	_muldivmod		; do */MOD code
_muldiv_return_to:
	pop	hl			; drop remainder
	ld	iy,_next		; set next return
	nxt

; Bytes:	22
; Notes:	This illustrates a sneaky way to use a primitive as a
;		subroutine. The */MOD code is executed as normal but
;		the JP {IY} return jumps back to the */ code rather than
;		NEXT. This code then restores the normal return to NEXT.








;;;
;;; */MOD
;;;

; TIL pg 112 (127)

; Class:	Arithmetic
; Function:	Does a signed multiply of the third stack word by the low-
;		order byte of the second stack entry and a signed divide of
;		the 24-bit product by the low-order byte of the top stack
;		entry. Replaces the three entries with the 16-bit quotient as
;		the second and the 8-bit residual as the top stack entry.
; Input/Output:	Three stack entries/Two stack entries
; Z80 Code:

	db	5,"*/M"
	__link__
muldivmod:
	dw	$+2
_muldivmod:
	pop	hl		; divisor to HL
	exx			; save IR and divisor
	pop	bc		; multiplicand (8)
	pop	de		; multiplier (16)
	call	_isign		; field * sign
	call	_udm		; do 16x8 multiply
	exx			; get divisor and IR
	ex	af,af'		; get / sign flag
	xor	l		; xor in divisor sign
	ex	af,af'		; save result sign
	ld	a,l		; get divisor
	exx			; save IR again
	and	a		; test divisor sign
	jp	p,_muldivmod_skip	; if +, it's ok
	neg			; make divisor +
_muldivmod_skip:
	ld	d,c		; move high 8 bits of 24
	ld	e,a		; move divisor
	call	_uddiv		; do 24x8 divide
	call	_osign		; justify result
	push	hl		; quotient to stack
	push	bc		; remainder to stack
	exx			; restore IR
	nxt

; Bytes:	43
; Notes:	The $*/MOD entrance is used by */. No tests are per¬
;		formed to insure valid number lengths.








;;;
;;; *C#
;;;

;  TIL pg 113 (128)

	; headerless

cliteral:
	dw	$+2
	ld	a,(bc)	; get byte at IR
	ld	e,a	; move it to DE
	inc	bc	; bump IR
	rla		; sign to carry
	sbc	a,a	; ff if neg else 00
	ld	d,a	; set sign extenstion
	push	de	; push to data stack
	nxt








;;;
;;; *C+LOOP
;;;

; TIL pg 113 (128)

; Class:	Program Control Directive (Headerless)
; Function:	Pops the top stack entry and increments the top return
;		stack byte by the low-order byte from the stack. Control is
;		then transferred to the *CLOOP code to mechanize a non¬
;		unity byte indexed loop.
; Input/Output:	One stack entry and one return stack byte/One return
;		stack byte.
; Usage:	Available only to the system.
; Z80 Code:

	; headerless
s_cploop:
	dw	$+2
	push	ix	; get return stack
	pop	hl	; pointer
	pop	de	; get inc byte
	ld	a,(hl)	; get loop count
	add	e	; and increment
	ld	(hl),a	; restore loop couunt
	jp	_cloop	; jump to *CLOPP code








;;;
;;; *CDO
;;;

; TIL 114 (129)

; Class:	Program Control Directive (Headerless)
; Function:	Moves the low-order byte of the top stack entry (the loop
;		start index) and the low-order byte of the second stack en-
;		try (the loop termination argument) to the return stack
;		with the start index as the top return stack entry and the
;		terminator as the second entry. This initializes the byte in¬
;		dexed loop.
; Input/Output:	Two stack entries/Two return stack byte entries.
; Usage:	Available only to the system.

	; headerless
s_cdo:	dw	$+2
	pop	hl	; get start index
	ld	(ix-2),l	; to return top
	pop	hl
	ld	(ix-1),l	; to return second
	dec	ix	; reset return
	dec	ix	; stack pointer
	nxt








;;;
;;; *CLEAVE
;;;

; Class:	Program Control Directive (Headerless)
; Function:	Replaces the top return stack byte (the byte loop index)
;		with the second return stack byte (the terminating argu¬
;		ment) to force loop exit on the next byte loop test.
; Input/Output:	Two return stack bytes/Two return stack bytes.
; Usage:	Available only to the system.
; Z80 Code:

	; headerless
s_cleave:
	dw	$+2
	ld	a,(ix+1)	; get terminator
	ld	(ix+0),a	; to index
	nxt

; Bytes:	10








;;;
;;; *CLOOP
;;;

; TIL pg 114-115 (129-130)

; Class:	Program Control Directive (Headerless)
; Function:	Increments the top return stack byte by 1 and compares it
;		to the second return stack byte entry. If the second byte is
;		larger than the first, a jump to the *WHILE code occurs to
;		implement a relative backwards jump. Otherwise the top
;		two return stack entries are dropped and the instruction
;		register is incremented by 1 to step past the relative jump
;		byte. Controls byte loop termination.
; Input/Output:	Two return stack bytes/Two return stack bytes except on
;		completion.
; Usage:	Available only to the system.
; Z80 Code:

s_cloop:	dw	$+2
	push	ix	; get return
	pop	hl	; stack pointer
	inc	(hl)	; increment index
_cloop:	ld	a,(hl)	; get index
	inc	hl	; point to terminator
	sub	(hl)	; index - terminator
	jp	c,s_while	; if CY = 1, jump back
	inc	ix	; else drop index
	inc	ix	; and terminator
	inc	bc	; increment IR
	nxt

; Bytes:	19
; Notes:	*C+LOOP uses the SCLOOP entrance.








;;;
;;; *DO
;;;

; TIL pg 115 (130)

; Class:	Program Control Directive (Headerless)
; Function:	Moves the top stack entry word (the loop start index) and
;		the second stack entry word (the loop terminating argu¬
;		ment) to the return stack with the start index as the top
;		return stack entry and the terminator as the second entry.
;		This initializes a word indexed loop.
; Input/Output:	Two stack entries/Two return stack word entries.
; Usage:	Available only to the system.
; Z80 Code:

	; headerless
s_do:	dw	$+2
	pop	hl	; set start index
	ld	(ix-4),l	; move to the return
	ld	(ix-3),h	; stack as top entry
	pop	hl	; get terminator
	ld	(ix-2),l	; move to the return
	ld	(ix-1),h	; stack as 2nd entry
	ld	de,-4		; reset return
	add	ix,de		; stack pointer
	nxt

; Bytes:	23








;;;
;;; *ELSE
;;;

; TIL pp 115,116 (130,131)

; Class:		Program Control Directive (Headerless)
; Function:	Increments the instruction register by the value whose
;		address is in the instruction register to effect a relative
;		forward jump.
; Input/Output:	None/None.
; Usage:		Available only to the system.`
; Z80 Code:

	; headerless
s_else:	dw	$+2
_s_else:
	ld	a,(bc)		; get jump byte
	add	c		; add it to ir
	ld	c,a		; reset ir
	jr	nc,_s_else_out	; past page?
	inc	b		; yes
_s_else_out:
	nxt

; Bytes:		10
; Notes:		The s_else entrance is used by s_if








;;;
;;; *END
;;;

;  TIL pg 116 (131)

; Class:		Program Control Directive (Headerless)
; Function:	If the top stack entry is 0, the instruction register is in¬
;		cremented by the value whose address is in the instruction
;		register to implement a relative backwards jump. Other¬
;		wise the instruction register is incremented by 1 to step
;		past the relative jump byte.
; Input/Outout:	One stack entry/None.
; Usage		Available only to the system.
; Z80 Code:

	; headerless
s_end:	dw	$+2
	pop	hl	; get the flag
	ld	a,l	; are all bits 0
	or	h	; or false
	jp	z,s_while	; if 0, jump
	inc	bc	; else bump IR
	nxt

; Bytes:		11
; Notes:		The jump to $WHILE evokes the backwards jump.








;;;
;;; *IF
;;;

; TIL pg 116 (131)

	; headerless
s_if:	dw	$+2
	pop	hl	; get the flag
	ld	a,l	; are all bit 0
	or	h	;
	jp	z,_s_else	; if 0, jump
	inc	bc	; else jump OR
	nxt








;;;
;;; *LEAVE
;;;

; TIL pg 117 (132)

; Class:	Program Control Directive (Headerless)
; Function:	Replaces the top return stack word (the word loop index)
;		with the second return stack word (the terminating argu¬
;		ment) to force loop exit on the next word loop test.
; Input/Output:	Two return stack words/Two return stack words.
; Usage:	Available only to the system.
; Z80 Code:

	; headerless
s_leave:	dw	$+2
	ld	a,(ix+3)	; get term log byte
	ld	(ix+1),a	; to index low byte
	ld	a,(ix+2)	; get term high byte
	ld	(ix+0),a	; to index high byte
	nxt

; Bytes:	16








;;;
;;; *LOOP
;;;

; TIL pg 117 (132)

; Class:	Program Control Directive (Headerless)
; Function:	Increments the top return stack word by 1 and compares it
;		to the second return stack word entry. If the second word
;		is larger than the first, a jump to the $WHILE code occurs
;		to implement a relative backwards jump. Otherwise the
;		top return stack entries are dropped and the instruction
;		register is incremented by 1 to step past the relative jump
;		byte. Controls word loop termination.
; Input/Output:	Two return stack words/Two return stack words except
;		on completion.
; Usage:	Available only to the system.
; Z80 Code;

	; headerless
s_loop:	dw	$+2
	push	ix		; get return
	pop	hl		; stack pointer
	ld	a,1		; get increment
_s_loop:
	add	(hl)		; inc index low
	ld	(hl),a		; restore low index
	inc	hl		; bump to index high
	jr	nc,_s_loop_page	; past page?
	inc	(hl)		; bump page
_s_loop_page:
	ld	d,(hl)		; get index high
	inc	hl		; bump to term low
	sub	(hl)		; index - term (low)
	ld	a,d		; index high to A
	inc	hl		; bump to term high
	sbc	a,(hl)		; index - term - CY (high)
	jp	c,_s_while	; if CY = 1, jump back
	ld	de,4		; else drop index
	add	ix,de		; and terminator
	inc	bc		; increment IR
	nxt

; Bytes:	30
; Notes:	*+LOOP uses the SLOOP entrance








;;;
;;; *STACK
;;;

; TIL pg 98 *(113)

; The ‘STACK routine is a primitive without a header. Like QUESTION,
; ‘STACK is a nonstructured routine in that it has a single entrance but a dual
; exit. ‘STACK tests for stack underflow. If an underflow condition does not
; exist, a normal exit to NEXT occurs. If underflow is detected, the stack pointer
; is reset to point to the correct top of stack address, the stack error message ad¬
; dress is loaded to the WA register and a jump to $PATCH is executed. This
; will patch the system and reinitialize the system before displaying the stack er¬
; ror message and reverting to the input submode. A Z80 assembly code listing
; for this routine is given in listing 5.7.

	; headerless
s_stack:	dw	$+2
	ld	hl,stack	; get top of stack address
	and	a	; reset the carry flag
	sbc	hl,sp	; subtract current SP
	jr	nc,_s_stack_ok	; if CY = 0, no underflow
	add	hl,sp	; else restore top address
	ld	sp,hl	; and reset stack pointer
	ld	de,stkmsg	; stack error message address to WA
	jp	_patch
_s_stack_ok:	nxt








;;;
;;; *SYS
;;;

; TIL pg 118 (133)

; Class:	System (Incomplete)
; Function:	Used by the system to recover the addresses of system
;		variables.
; Input/Output:	See notes.
; Usage: Available only to the system.
; Z80 Code:

_sys:	ld	a,(de)		; DE = WA, @WA = offset
	ld	hl,sys_block	; start of sysblock
	add	l		; add offset
	ld	l,a		; variable address lo
	push	hl		; address to stack
	nxt

; Bytes:	9
; Notes:	This code is the generic code for a user block type defined
;		keyword without the header-creating code.
;		All system variables defined in the system block contain
;		$SYS as their code address followed by a 1-byte offset as
;		their code body. The offset points to the system variable,
;		relative to the start of the block. A full 256-byte block is
;		not reserved for system variables (only 20 thru 30 bytes are
;		used), which is why there is no defining code. A possibility
;		exists for overwriting system code if this were allowed.
;		All system variables are predefined.








;;;
;;; *WHILE
;;;

;  TIL pg 118 (133)

; Class:		Program Control Directive (Headerless)
; Function:	Increments the instruction register by the value whose ad¬
;		dress is in the current instruction register to implement a
;		relative backwards jump.
; Input/Output:	None/None.
; Usage:		Available only to the system.

	; headerless
s_while:
	dw	$+2
_s_while:
	ld	a,(bc)		; get jump byre
	add	c		; add it to IR
	ld	c,a		; reset IR
	jr	c,_s_while_out	; past page?
	dec	b		; yes
_s_while_out:	nxt

; Bytes:		10
; Notes:		The $WHILE entrance is used by *END, *LOOP, and
;		*CLOOP to execute the backward jump.








;;;
;;; *[
;;;

; TIL pg 119 (134)

; Class:	Literal Handler (Headerless)
; Function:	Uses the Instruction Register (IR) as a pointer to a string
;		embedded in the threaded code. Extracts the string length
;		from the first byte pointed to by the IR and outputs that
;		many characters to the display. Leaves the IR pointing to
;		the first byte past the embedded string.
; Input/Output:	None/None.
; Usage:	Available only to the system
; Z80 Code:

s_string:
	dw	$+2
	ld	a,(bc)		; BC = ir, @IR = length
	ld	d,a		; save length
_s_string_loop:
	inc	bc		; bump IR
	ld	a,(bc)		; get at IR
	call	_echo		; echo character
	dec	d		; decrement length
	jr	nz,_s_string_loop	; loop until length == 0
	inc	bc		; ajdust IR
	nxt

; Bytes:	15








;;;
;;; +
;;;

; TIL pg 119 (134)

; Class:	Literal Handler (Headerless)
; Function:	Uses the Instruction Register (IR) as a pointer to a string
;		embedded in the threaded code. Extracts the string length
;		from the first byte pointed to by the IR and outputs that
;		many characters to the display. Leaves the IR pointing to
;		the first byte past the embedded string.
; Input/Output:	None/None.
; Usage:	Available only to the system.
; Z80 Code:

	db	1,"+  "
	__link__
plus:	dw	$+2
	pop	hl	; get first word
	pop	de	; get second word
	add	hl,de	; add them
	push	hl	; push sum
	nxt

; Bytes:	15








;;;
;;; +!
;;;

; TIL pp 119-120 (134-135)

; Class:	Memory Reference
; Function:	Pops two stack entries and adds the word at the second en¬
;		try to the word whose address is the top entry.
; Input/Output:	Two stack entries/None.
; Usage:	Incrementing and decrementing word length data stored in
;		programmable memory.
; Z80 Code:

	db	2,"+! "
	__link__
plusstore:
	dw	$+2
	pop	hl	; get address
	pop	de	; get inc/dec
	ld	a,(hl)	; get low byte
	add	e	; inc/dec low byte
	ld	(hl),a	; store it back
	inc	hl	; step to high byte
	ld	a,(hl)	; get high byte
	adc	a,d	; inc/dec high byte
	ld	(hl),a	; store it back
	nxt








;;;
;;; +LOOP
;;;

; Class:	Compiler Directive (Immediate)
; Function:	Adds the word address of the program control directive
;		*+LOOP to the dictionary, then computes the difference
;		between the current free dictionary address and the ad¬
;		dress at the top of the stack and encloses the low-order
;		byte in the dictionary as the relative jump byte.
; Input/Output:	One stack entry/None.
; Usage:	Used to terminate a DO . . . -I-LOOP construct in the com¬
;		pile mode.
; Code:

	db	5,"+LO"
	__ilink__
plusloop:
	dw	_s_colon
	dw	literal
	dw	s_plusloop
	dw	endcomma
	dw	s_semi

; Bytes:	16
; Formal Definition:
;	: +LOOP ' *+LOOP END, ; IMMEDIATE








;;;
;;; +SP
;;;

; Class:	System
; Function:	Adds the current stack pointer to the number at the top of
;		the stack.
; Input/Output:	One stack entry/One stack entry.
; Usage:	Direct addressing of data on the stack
; Z80 Code:

	db	3,"+SP"
	__link__
plussp:	dw	$+2
	pop	hl	; get number
	add	hl,sp	; add stack pointer
	push	hl	; restore pointer
	nxt

; Bytes 13








;;;
;;; , (comma)
;;;

;  TIL pg 121 (136)

; Class:	System
; Function:	Pops the top stack entry word and encloses it in the free
;		dictionary space.
; Input/Output:	One stack entry/None.
; Usage:	Used to build dictionary keywords.
; Z80 Code:

	db	1,",  "
	__link__
comma:	dw	$+2
	pop	de	; get word
	ld	hl,(s_dp)	; get @DP
	ld	(hl),e	; store low byte
	inc	hl	; bump @DP
	ld	(hl),d	; store high byte
	inc	hl	; bump @DP
	ld	(s_dp),hl	; update @DP
	nxt

; Bytes:		21








;;;
;;; -
;;;

; TIL pg 121 (136)

; Class: 	Arithmetic
; Function:	Pops the top two stack entries and two's complement sub¬
;		tracts the top entry from the second entry and pushes the
;		result.
; Input/Output:	Two stack entries/One stack entry.
; Z90 Code:

	db	1,"-  "
	__link__
sub:	dw	$+2
	pop	de	; get B
	pop	hl	; get A;
	and	a	; reset carry
	sbc	hl,de	; form A-B
	push	hl	; push result
	nxt

; Bytes:	16
; Notes:	No test of overflow or carry are made.








;;;
;;; -1
;;;

; TIL not defined

	db	2,"-1 "
	__link__
subone:	dw	$+2
	ld	de, -1
	push	de
	nxt








;;;
;;; -SP
;;;

; TIL pg 131 (136)

; Class:	System
; Function:	Subtracts the current stack pointer from the number at the
;		top of the stack.
; Input/Output:	One stack entry/One stack entry.
; Usage:	Direct addressing of stack data.
; Z80 Code:

	db	3,"-SP"
	__link__
subsp:
	dw	$+2
	pop	hl	; get the number
	and	a	; reset carry
	sbc	hl,sp	; subtract stack pointer
	push	hl	; push pointer
	nxt

; Bytes:	15








;;;
;;; .
;;;

; TIL pg 122 (137)


; Class:	I/O
; Function:	Displays the top stack entry number to the operator (given
;		the current number base) and follows by a space. Destroys
;		the top stack entry in the process.
; Input/Output:	One stack entry/None.
; Usage:	Displaying signed numbers to the operator.
; Code:
;		 <#	;INITIALIZE CONVERSION
;		ABS	;TAKE THE ABSOLUTE VALUE
;		#S 	;CONVERT ABSOLUTE VALUE
;		SIGN	;ADD - SIGN IF REQUIRED
;		#>	;DISPLAY RESULT

	db	1,".  "
	__link__
dot:	dw	_s_colon
	dw	sharpbegin
	dw	abs
	dw	sharps
	dw	sign
	dw	sharpend
	dw	s_semi

; Bytes: 20
; Formal Definition:
;		: . <# ABS #S SIGN #> ;








;;;
;;; .R
;;;

; TIL pg 122 (137)

; Class:	I/O
; Function:	Displays the second stack number to the operator (given
;		the current system number base) in a field width deter¬
;		mined by the top stack entry. The number is right adjusted
;		in the field and followed by a space. The field width is the
;		minimum field width.
; Input/Output:	Two stack entries/None.
; Usage:	Formatting display number output.
; Code:

	db	2,".R "
	__link__
dotr:	dw	_s_colon
	dw	twomul	; double character counnt ( covert to cells )
	dw	subsp		; subtract current stack pointer
	dw	tor		; save as temporary
	dw	sharpbegin	; initialize conversion (save sign)
	dw	abs		; convert number to positive value
	dw	sharps		; convert to a string
	dw	sign		; add sign if negative
	dw	crfrom		; get sign from temproary
	dw	drop		; drop it
	dw	rfrom		; get termporary
	dw	plussp		; add current stack pointer
	dw	twodivide	; back from cells to characters
	dw	pad		; add spaces if required
	dw	display		; display result
	dw	s_semi

; Bytes:	36
; Formal Definition:
;	: .R 2* -SP <R <# ABS #S SIGN CR> DROP R> +SP PAD DOSPLAY ;









;;;
;;; /
;;;

; TIL pg 123 (138)

; Class:	Arithmetic
; Function:	Does a signed divide of the second stack word by the loworder byte of the top stack entry. Replaces both entries
;		with an 8-bit quotient expanded to 16 bits.
; Input/Output:	Two stack entries/One stack entry.
; Usage:	Signed integer arithmetic.
; Z80 Code:

	db	1,"/  "
	__link__
div:	dw	$+2
	exx		; save IR
	pop	de	; get divisor (8 bits)
	pop	bc	; get dividend (16 bits)
	call	_isign	; field input signs
	call	_usdiv	; divide 16x8
	call	_osign	; justify result
	push	hl	; quotient to stack
	exx		; restore IR
	nxt








;;;
;;; /MOD
;;;

; TIL pg 123 (138)

; Class:	Arithmetic
; Function:	Does a signed divide of the second stack entry by the low-
;		order byte of the top stack entry. Replaces these entries
;		with the 8-bit quotient expanded to 16 bits as the second
;		entry and the positive 8-bit remainder expanded to 16 bits
;		as the top entry.
; Input/Output:	Two stack entries/Two stack entries.
; Usage:	Signed integer arithmetic.
; Z80 Code:

	db	4,"/MO"
	__link__
divmod:
	dw	$+2
	exx		; save IR
	pop	de	; get divisor (8 bits)
	pop	bc	; get dividend (16 bits)
	call	_isign	; field input signs
	call	_usdiv	; divide 16x8
	call	_osign	; justify resuult
	push	hl	; quotient to stack
	push	bc	; remainder to stack
	exx		; restore IR
	nxt

; Bytes:	25
; Notes:	Does not test the top stack entry to insure it is a valid 8-bit
;		number. No test is made to insure a valid 8-bit quotient.








;;;
;;; 0
;;;

; TIL not defined

	db	1,"0  "
	__link__
zero:	dw	$+2
	ld	de, 0
	push	de
	nxt








;;;
;;; 0<
;;;

; TIL pg 124 (139)

; Class:	Relational
; Function:	If the top stack entry is two's complement negative, it is
;		replaced by a True flag. Otherwise it is replaced by a False
;		flag.
; Input/output:	One stack entry/One stack entry.
; Usage:	Test conditioning prior to branching
; Z80 Code:

	db	2,"0< "
	__link__
zless:	dw	$+2
	pop	af		; get number
	ld	de,0		; set flag false
	rla			; sign to CY
	jr	nc,_zless_push	; if CY == 0, push false
	inc	e		; else flag true
_zless_push:
	push	de		; flag to stack
	nxt

; Bytes:	19








;;;
;;; 0=
;;;

; Class:	Relational
; Function:	If the top stack value is 0, it is replaced by a True flag.
;		Otherwise it is replaced by a False flag.
; Input/Output:	One stack entry/One stack entry.
; Usage:	Test conditioning prior to branching.
; Z80 Code:

	db	2,"0= "
	__link__
zeq:	dw	$+2
	pop	hl		; get word
	ld	a,l		; move low byte
	or	h		; or in high byte
	ld	de,0		; get false
	jr	nz,_zeq1	; not zero pushes false
	inc	de		; else make flag true
_zeq1:	push	de		; push flag
	nxt








;;;
;;; 0SET
;;;

; TIL pg 124 (139)

; Class:	Memory Reference
; Function:	Pops the top stack entry and sets the word whose address
;		was the top entry to 0.
; Input/Output:	One stack entry/None.
; Usage:	Initializing word length data in programmable memory to
;		0 or setting word length flags in programmable memory to
;		False.

	db	4,"0SE"
	__link__
zset:	dw	$+2
	pop	hl	; get address
	xor	a	; zeros A register
	ld	(hl),a	; zero low byte
	inc	hl	; dump address pointer
	ld	(hl),a	; zero high byte
	nxt

; Bytes:	15








;;;
;;; 1
;;;

; TIL not defined

	db	1,"1  "
	__link__
one:	dw	$+2
	ld	de, 1
	push	de
	nxt








;;;
;;; 1+
;;;

; TIL pg 125 (140)

; Class:	Arithmetic
; Function:	Increments the top stack entry by 1.
; Input/OUtput:	One stack entry/One stack entry.
; Usage:	Signed arithmetic, byte addressing and index incrementing.
; Z80 Code:

	db	2,"1+ "
	__link__
oneplus:
	dw	$+2
	pop	hl	; get word
	inc	hl	; bump it
	push	hl	; restore it
	nxt
; Bytes:	13
; Notes:	No tests of overflow or carry are made.








;;;
;;; 1-
;;;

; TIL pg 125 (140)

; Class:	Arithmetic
; Function:	Decrements the top stack entry by 1.
; Input/OUtput:	One stack entry/One stack entry.
; Usage:	Signed arithmetic, byte addressing and index decrementing.
; Z80 Code:

	db	2,"1- "
	__link__
onesub:
	dw	$+2
	pop	hl	; get word
	dec	hl	; decrement it
	push	hl	; restore it
	nxt

; Bytes:	13
; Notes:	No tests of overflow or carry are made.








;;;
;;; 1SET
;;;

; TIL pg 125-126 (140-141)

; Class:	Memory Reference
; Function:	Pops the top stack entry and sets the word whose address
;		was the top entry to one.
; Input/Output:	One stack entry/None.
; Usage:	Initializing word length data in programmable memory to
;		one or setting word length flags in programmable memory to
;		True.

	db	4,"1SE"
	__link__
oneset:	dw	$+2
	pop	hl	; get address
	ld	(hl),1	; 1 set low byte
	inc	hl	; dump address pointer
	ld	(hl),0	; 0 set high byte
	nxt

; Bytes:	16







;;;
;;; 2*
;;;

; TIL pg 126 (141)

; Class:	Arithmetic
; Function:	Multiplies the top stack entry by 2.
; Input/Output:	One stack entry/One stack entry.
; Usage:	Signed integer arithmetic
; Z80 Code:

	db	2,"2* "
	__link__
twomul:
	dw	$+2
	pop	hl	; get word
	add	hl,hl	; double it
	push	hl	; restore it
	nxt

; Bytes:	13
; Notes:	No tests of overflow or carry are made.








;;;
;;; 2+
;;;

; TIL pg 126 (141)

; Class:	Arithmetic
; Function:	Increments the word at the top of the stack by 2.
; Input/Output:	One stack entry/One stack entry.
; Usage:	Signed integer arithmetic
; Z80 Code:

	db	2,"2+ "
	__link__
twoplus:
	dw	$+2
	pop	hl	; get word
	inc	hl	; word + 1
	inc	hl	; word + 2
	push	hl	; push word + 2
	nxt

; Bytes:	13
; Notes:	No tests of overflow or carry are made.








;;;
;;; 2-
;;;

; TIL pg 126 (141)

; Class:	Arithmetic
; Function:	Decrements the word at the top of the stack by 2.
; Input/Output:	One stack entry/One stack entry.
; Usage:	Signed integer arithmetic
; Z80 Code:

	db	2,"2- "
	__link__
twosub:
	dw	$+2
	pop	hl	; get word
	dec	hl	; word - 1
	dec	hl	; word - 2
	push	hl	; push word +-2
	nxt

; Bytes:	13
; Notes:	No tests of overflow or carry are made.








;;;
;;; 2/
;;;

; TIL pg 127 (142)

; Class:	Arithmetic
; Function:	Divides (signed) the top stack word by 2.
; Input/Output:	One stack entry/One stack entry.
; Usage:	Signed integer arithmetic.
; Z80 Code:

	db	2,"2/ "
	__link__
twodivide:
	dw	$+2
	pop	hl	; get word
	sra	h	; arithmetic shight
	rr	l	; propagate CY
	push	hl	; push word/2
	nxt

; Bytes:	16








;;;
;;; 2DUP
;;;

; TIL pg 127 (142)

; Class:	Stack
; Function:	Duplicates the top stack entry twice.
; Input/Output:	One stack entry/Three stack entries.
; Usage:	Duplication of data on the stack.
; Z80 Code:

	db	4,"2DU"
	__link__
twodup:	dw	$+2
	pop	hl	; get word
	push	hl	; restore it
	push	hl	; dup it
	push	hl	; dup it again
	nxt








;;;
;;; 2OVER
;;;

; TIL pg 127 (142)

; Class:	Stack
; FUnction:	Duplicates the third stack entry over the top two and
;		pushes the word to the stack.
; Input/Output:	Three stack entries/Four stack entries.
; Usage:	Stack data management.
; Z80 Code:

	db	5,"2OV"
	__link__
twoover:
	dw	$+2
	exx		; save IR
	pop	hl	; get top
	pop	de	; get 2nd
	pop	bc	; get 3rd
	push	bc	; push 3rd
	push	de	; push 2nd
	push	hl	; push top
	push	bc	; push 3rd to top
	exx		; resotre IR
	nxt








;;;
;;; 2SWAP
;;;

; TIL pg 128 (143)

; Class:	Stack
; Function:	Interchanges the top and third stack words.
; Input/Output:	Three stack entries/Three stack entries.
; Usage:	Stack management.
; Z80 Code:

	db	5,"2SW"
	__link__
twoswap:
	dw	$+2
	pop	hl	; get top
	pop	de	; get 2nd
	ex	(sp),hl	; top to stack
	push	de	; restore 2nd
	push	hl	; 3rd to top
	nxt

; Bytes:	15








;;;
;;; :
;;;

; TIL pg 128 (143)


; Class:	Defining Word
; Function:	Sets the CONTEXT vocabulary equal to the CURRENT
;		vocabulary, creates a secondary header for the token
;		following in the input buffer and links it to the CUR¬
;		RENT vocabulary and sets the system mode to the compile
;		mode.
; Input/Output:	None/None.
; Usage:	Initiate compilation of secondary keywords.
; Code:

	db	1,":  "
	__link__
colon:	dw	_s_colon
	dw	current	; current address
	dw	at	; contains vocabulary address
	dw	context	; context address
	dw	store	; current into context
	dw	create	; create primitive header
	dw	literal	; address of colon routine
	dw	_s_colon	;
	dw	castore	; replace code address
	dw	mode	; mode address
	dw	c1set	; set compile mode (mode = 1)
	dw	s_semi

; Bytes:	30
; Formal Definition:
;	: : CURRENT @ CONTEXT ! ' _s_colon CA! MODE C1SET ;








;;;
;;; ;
;;;

; TIL pg 128-129 (143-144)

; Class:	Compile Mode Termination Directive (Immediate)
; FUnction:	Encloses the word address of the inner interpreter SEMI
;		routine in the dictionary and sets the system mode to the
;		execute mode.
; Input/Output:	None/None.
; Usage:	Terminates the definition of a secondary and re-establishes
;		the execute mode.

	db	1,";  "
	__ilink__
semi:	dw	_s_colon
	dw	literal	; address of semi routine
	dw	s_semi
	dw	comma	; enclose it in the dictionary
	dw	mode	; mode address
	dw	c0set	; set execute mode (mode = 0)
	dw	s_semi








;;;
;;; ;CODE
;;;

; TIL pg 129 (144)

; Class:	Compile Mode Termination Directive (Immediate)
; Function:	Encloses the word address of the keyword SCODE in the
;		dictionary and sets the system mode to execute.
; Input/Output:	None/None.
; Usage:	Terminates a defining keyword definition. Always fol¬
;		lowed by generic machine code that defines the execution
;		time action of the defined class.
; Code:

	db	5,";CO"
	__ilink__
semicode:
	dw	_s_colon
	dw	literal	; word address of scode
	dw	scode
	dw	comma	; enclose it in the dictionary
	dw	mode	; mode address
	dw	c0set	; set execute mode (mode = 0)
	dw	s_semi

; Bytes:	20
; Formal Definition:
;		: ;CODE ' ;CODE , MODE COSET ; IMMEDIATE








;;;
;;; <
;;;

; TIL pg 129 (144)

; Class:	Relational
; Function:	If the second stack entry is less than the top entry, both en¬
;		tries are replaced by a True flag. Otherwise both are
;		replaced by a False flag.
; Input/Output:	Two stack entries/One stack entry.
; Usage:	Test conditioning prior to branching.
; Z80 Code:

	db	1,"<  "
	__link__
less:	dw	$+2
	pop	de	; get top
	pop	hl	; get 2nd
	and	a	; reset carry
	sbc	hl,de	; 2nd - top
	ld	de,0	; set flag false
	jp	p,less1	; if positive, false
	inc	e	; set flag true
less1:	push	de	; flag to stack
	nxt

; Bytes:	23








;;;
;;; <#
;;;

; TIL pg 130 (145)

	db	2,"<# "
	__link__
sharpbegin:
	dw	$+2
	pop	hl	; get the number
	ld	e,0xa0	; space with B7=1
	push	de	; push string stop
	push	hl	; restore number
	dec	ix	; dec RSP
	ld	(ix+0),h	; sign to return
	nxt

; Bytes:	20
; Notes:	Must be followed by a #> or a CR> whithin a definition to
;		clean up the return stack and leave a valid return address
;		on the stack.








;;;
;;; <BUILDS
;;;

; TIL pg 130 (145)

; Class:	Defining Word
; Function:	Creates a CONSTANT keyword definition with an initial
;		value of 0. The keyword name is the next available token
;		in the input buffer when < BUILDS is executed.
; Input/Output:	None/None.
; Usage:	Used to initiate a high-level defining word which must later
;		be terminated with a DOES > .
; Code:

	db	7,"<BU"
	__link__
builds:	dw	_s_colon
	dw	zero	; initial value
	dw	constant	; creates a constant keyword
	dw	s_semi

; Bytes:	14
; Notes:	See Section 4.5.5.
; Formal Definition:
;		: <BUILDS O CONSTANT ;








;;;
;;; <R
;;;

; TIL pg 130-131 (145-146)

; Class:	Interstack
; Function:	Pops the top stack word and pushes it to the return stack.
; Input/Output:	One stack entry/One return stack word entry.
; Usage:	Temporary storage of data within a definition or direct
;		return stack control.

; Z80 Code:

	db	2,"<R "
	__link__
tor:	dw	$+2
	pop	hl	; get word
	dec	ix	; push it to the return stack
	ld	(ix+0),h
	dec	ix
	ld	(ix+0),l
	nxt








;;;
;;; =
;;;

; TIL pg 131 (146)

; Class:		Relational
; Function:	If the top two stack entries are equal, both are replaced by
;		a True flag. Otherwise both are replaced by a False flag.
; Input/Output:	Two stack entries/One stack entry.
; Usage:		Test conditioning prior to branching.
; Z80 Code:

	db	1,"=  "
	__link__
equals:	dw	$+2
	pop	hl	; get top
	pop	de	; get 2nd
	and	a	; reset carry
	sbc	hl,de	; top-2nd
	ld	de,0	; set flag false
	jr	nz,equals1	; if =, push false
	inc	e	; set flag true
equals1:
	push	de	; flag to stack
	nxt

; Bytes:	22








;;;
;;; >
;;;

; TIL pg 131 (146)

; Class:	Relational
; Function:	If the second stack entry is greater than the top entry, both
;		entries are replaced by a True flag. Otherwise both are
;		replaced by a False flag.
; Input/Output:	Two stack entries/One stack entry.
; Usage:	Test conditioning prior to branching.
; Z80 Code:

	db	1,">  "
	__link__
greater:
	dw	$+2
	pop	hl	; get top
	pop	de	; get 2nd
	and	a	; reset carry
	sbc	hl,de	; 2nd - top
	ld	de,0	; set flag false
	jp	p,_greater1	; if positive, false
	inc	e	; set flag true
_greater1:
	push	de	; flag to stack
	nxt

; Bytes:	23








;;;
;;; ?
;;;

; TIL pg 132 (147)

; Class:	I/O
; Function:	Displays to the operator (using the current system number
;		base) the word whose address is the top stack entry.
;		Number display is always followed by a space.
; Input/Output:	One stack entry/None.
; Usage:	Displaying signed numbers to the operator, generally the
;		contents of variables.

	db	1,"?  "
	__link__
q:	dw	_s_colon
	dw	at	; get the number
	dw	dot	; display it
	dw	s_semi

; Bytes:	14








;;;
;;; ?EXECUTE
;;

; TIL pg 82-83 (97-98)

;  : ?EXECUTE          \ AD
;      STATE C@        \ FG AD
;      STATE C0SET     \ FG AD
;      MODE C@         \ FG FG AD
;      = IF            \ AD
;         EXECUTE      \
;         *STACK       \
;      ELSE            \ AD
;         ,            \
;      THEN
;  ;

	; headerless
q_execute:
	dw	_s_colon
	dw	state
	dw	cat
	dw	state
	dw	c0set
	dw	mode
	dw	cat
	dw	equals
	dw	s_if
	db	q_ex1-$
	dw	execute
	dw	s_stack
	dw	s_else
	db	q_ex2-$
q_ex1:	dw	comma
q_ex2:	dw	s_semi








;;;
;;; ?NUMBER
;;;

;  TIL pp 83-84 (98-99)

; A secondary with no header. This routine attempts to convert
; the token located at the free dictionary space to a binary number using the cur¬
; rent system number base. If the conversion is unsuccessful, a True flag is
; pushed to the stack. This will occur if the token is the terminator or if the
; token is unidentifiable. If a successful conversion occurs, the system MODE
; flag is checked. If the MODE flag is True, a literal handler plus a number must
; be added to the dictionary. If the number is within the byte number range, the
; word address of the byte number literal handler *C# is added to the dictionary
; followed by the byte number. If the number is not within a byte range, the
; word address of the word number literal handler *# is added to the dictionary
; followed by the number. After the literal handler and number entry to the dic¬
; tionary, a False flag is pushed to the stack. If the conversion is successful and if
; the system MODE flag is False, the number is pushed to the stack and a False
; flag is pushed to the stack. This can leave an excess number on the stack which
; is exactly the right answer in the execute mode. The thread code design for
; ?NUMBER is shown in figure 5.5

;  : ?NUMBER
;    NUMBER IF
;      MODE C@ IF
;        SINGLE IF
;          ' *# ,
;          ,
;        ELSE
;          ' *C# ,
;          C,
;        THEN
;      THEN
;      0
;    ELSE
;      1
;    THEN
;   ;

	db	7,"?NU"
	__link__
qnumber:
	dw	_s_colon
	dw	number					; T N or F
	dw	s_if					; N
	db	qn4-$
	dw		mode				; MODE N
	dw		cat				; @MODE N
	dw		s_if				; N
	db		qn3-$
	dw			single			; flag N
	dw			s_if			; N
	db			qn1-$
	dw				literal		; 'literal N
	dw				literal
	dw				comma		; N
	dw				comma		; --
	dw			s_else			; N
	db			qn2-$
qn1:	dw				literal		; 'cliteral N
	dw				cliteral
	dw				comma		; N
	dw				ccomma		; --
qn2:	;			then
qn3:	;		then
	dw		zero				; 0
	dw	s_else					; --
	db	qn5-$
qn4:	dw		one				; 1
qn5:	dw	s_semi








;;;
;;; ?RS
;;;

; TIL pg 132 (147)

; Class:	System
; Function:	Pushes to the stack the current return stack pointer.
; Input/Output:	None/One stack entry.
; Usage:	Return stack display and control.
; Code:

	db	3,"?RS"
	__link__
qrs:	dw	$+2
	push	ix	; push return pointer
	nxt

; Bytes:	12








;;;
;;; ?SEARCH
;;;

; TIL pg 81i-82 (96-97)

; A secondary with no header. This routine will first search the
; context vocabulary — trying to locate a keyword whose header matches the
; token length and descriptor characters of the string moved to the dictionary
; space by TOKEN. The system variable CONTEXT contains the address of the
; appropriate context vocabulary. If the search is successful, the keyword word
; address is returned to the stack as the second stack entry, and a False flag is
; returned as the top stack entry. If the context vocabulary search is unsuc¬
; cessful, the system MODE flag is tested. If the MODE flag is False (execute
; mode), a single True flag is returned at the top of the stack. If the MODE flag is
; True (compile mode), the COMPILER vocabulary is searched. If the search is
; successful, the word address of the located keyword is returned to the stack as
; the second stack entry, a False flag is returned as the top stack entry and the
; system flag STATE is set True. If the compiler vocabulary search is unsuc¬
; cessful, a single True flag is returned as the top stack entry. The threaded code
; for ?SEARCH is shown in figure 5.3. A flag is always the top stack entry when
; ?SEARCH completes. If this flag is False, the word address of a located
; keyword is the second stack entry as a parameter for ?EXECUTE.

;  : ?SEARCH
;    CONTEXT @ @
;    SEARCH
;    DUP IF
;      MODE C@ IF
;        DROP
;        COMPILER @ SEARCH
;        DUP IF
;          0
;        ELSE
;          1
;        THEN
;        STATE C!
;      THEN
;    THEN
;  ;

	; headerless
qsearch:
	dw	_s_colon
	dw	context
	dw	at
	dw	at
	dw	search
	dw	dup
	dw	s_if
	db	qs_3-$
	dw	mode
	dw	cat
	dw	s_if
	db	qs_3-$
	dw	drop
	dw	compiler
	dw	at
	dw	search
	dw	dup
	dw	s_if
	db	qs_1-$
	dw	zero
	dw	s_else
	db	qs_2-$
qs_1:	dw	one
qs_2:	dw	state
	dw	cstore
qs_3:	dw	s_semi








;;;
;;; ?SP
;;;

; Class:	System
; Function:	Pushes to the stack the address of the top stack entry prior
;		to the execution of ?SP. If underflow occurs, the stack is
;		reset prior to the push.
; Input/Output:	None/One stack entry.
; Usage:	Data stack display and control.
; Z80 Code:

	db	3,"?SP"
	__link__
qsp:	dw	$+2
	ld	hl,0	; get stack
	add	hl,sp	;   pointer
	ex	de,hl	;
	ld	hl,stack	; get end of stack
	and	a	; reset carry
	sbc	hl,de	; end - SP
	jr	nc,qsp1	; nc is ok stack
	ld	sp,stack ; else init stack
qsp1:	push	de	; push prior SP
	nxt

; Bytes:	27








;;;
;;; @
;;;

;  TIL pg 133 (148)
;
; Class:	Memory Reference
; Function:	Replaces the address at the top of the stack with the word
;		at that address.
; Input/Output:	One stack entry/One stack entry.
; Usage:	Returns word length data stored in memory.
; Code:

	db	1,"@  "
	__link__
at:	dw	$+2
	pop	hl	; get the address
	ld	e,(hl)	; low byte at address
	inc	hl	; bump address
	ld	d,(hl)	; high byte
	push	de	; push contents
	nxt

; Bytes:	15
; Notes:	Low-byte, high-byte order is central processing unit
;		dependent.








;;;
;;; ABORT
;;;

; TIL pg 133 (148)

; Class:	System
; Function:	Does an unconditional jump to the START/RESTART
;		routine to re-initialize the system and the stacks.
; Input/Output:	None/None.
; Usage:	Used when the operator is totally at sea. (The system
;		knows exactly what's going on.)

	db	5,"ABO"
	__link__
abort:	dw	_start	; to start/restart








;;;
;;; ABS
;;;

	db	3,"ABS"
	__link__
abs:	dw	$+2
	pop	de	; get number
	bit	7,d	; if positive, Z = 1
	jr	z,abs1	; if z = 1, it's ok
	ld	hl,0	; else get a zero
	and	a	; reset carry
	sbc	hl,de	; zero - number
	ex	de,hl	; is positive
abs1:	push	de	; positive number
	nxt
; Bytes:	23








;;;
;;; ADUMP
;;;

; Class:	I/O
; Function:	Does a memory dump taking the second stack entry as the
;		starting address and the top entry as the ending address.
;		Displays a line consisting of the address, eight characters
;		of ASCII, a space, and eight more characters of ASCII.
;		Control ASCII codes are not displayed. Removes both en¬
;		tries.
; Input/Output:	Two stack entries/None.
; Usage:	Examining memory to locate or display string data
; Code:

	db	5,"ADU"
	__link__
adump:	dw	_s_colon
	dw	over	; prepare for loop start index
	dw	s_do	; initialize do loop
adump1:	dw	cret	; issue CR LF
	dw	dup	; duplicate line address
	dw	cliteral	; four character line address
	db	4
	dw	dotr	; print line address
	dw	apart	; issue first 8 characters
	dw	apart	; issue second 8 characters
	dw	wait	; time to stop and wait?
	dw	cliteral	; number of characters as index
	db	16
	dw	s_plusloop	; loop until done
	db	adump1-$
	dw	drop	; drop address pointer
	dw	s_semi

; Bytes:	17
; Formal Defintion:
;		: ADUMP OVER DO CRET DUP 4 .R APART APART WAIT 10 +LOOP DROP ;








;;;
;;; AND
;;;

; TIL pg 134-135 (149i-150)

; Class:	Logical
; Function:	Pops the top two stack words, does a logical AND of all
;		bits on a bit-by-bit basis and pushes the result to the stack.
; Input/Output:	Two stack entries/One stack entry.
; Usage:	Logical operations
; Z80 Code:

	db	3,"AND"
	__link__
and:	dw	$+2
	pop	hl	; get top
	pop	de	; get 2nd
	ld	a,l	; and low bytes
	and	e	;
	ld	l,a	; back to l
	ld	a,h	; and high bytes
	and	d	;
	ld	h,a	; back to h
	push	hl	; result to stack
	nxt

; Bytes:	19








;;;
;;; APART
;;;

; Class:	I/O
; Function:	Displays eight characters of ASCII using the top stack en¬
;		try as a pointer. The pointer is incremented with each
;		character access. ASCII control code is converted to a
;		displayable form before being echoed.
; Input/Output:	One stack entry/One stack entry.
; Usage:	Displaying memory.
; Code:

	db	5,"APA"
	__link__
apart:	dw	_s_colon
	dw	space	; format control
	dw	cliteral	; loop ending index
	db	8
	dw	zero	; loop starting index
	dw	s_cdo	; initiate display loop
apart1:	dw	dup	; duplicate pointer
	dw	cat	; get memory byte
	dw	cliteral	; to set MSB to 1
	db	0x80
	dw	ior	; make control code displayable
	dw	echo	; display byte as ascii
	dw	space	; space between charaters
	dw	oneplus	; increment pointer
	dw	s_cloop	; loop until done
	db	apart1-$
	dw	s_semi

; Bytes:	37
; Formal Definition:
;		: APART SPACE 8 0 CDO DUP C@ 80 OR ECHO SPACE 1+ CLOOP ;








;;;
;;; ASCII
;;;

; TIL pg 135 (150)

; Class: I/O
; Function:	Converts the low-order byte of the top stack entry from a
;		binary number to an ASCII code in the set 0 thru 9, A thru
;		Z.
; Input/Output:	One stack entry/One stack entry.
; Usage:	Converts binary numbers to their equivalent ASCII code
;		for conversion to displayable formats.
; Z80 Code:

	db	5,"ASC"
	__link__
ascii:	dw	$+2
	pop	hl	; get binary
	ld	a,'0'	; ascii 0
	add	l	; add binary
	cp	0x3a	; letter?
	jr	c,asout	; if cy == 1, a digit
	add	7	; add letter bias
asout:	ld	l,a	; back to L
	push	hl	; ascii code to stack
	nxt

; Bytes:	22








;;;
;;; ASPACE
;;;

; TIL pg 136 (151)

; Class:	 System
; Function: 	Pushes an ASCII space code to the low-order byte of the
;		stack.
; Input/Output: None/One stack entry.
; Usage: 	The normal token separator and to insert blanks in formatted displays.
; Code: 	Not applicable.
; Bytes:	9
; Formal Definition:
;		HEX 20 CCONSTANT ASPACE

	db	6,"ASP"
	__link__
aspace:	dw	s_cconstant
	db	' '








;;;
;;; BASE
;;;

; TIL pg 136 (151)

;  1 SYS BASE

	db	4,"BAS"
	__link__
base:	dw	_sys
	db	s_base-sys_block

; Bytes:	9
; Notes:	In the SYS users block. The code body is an offset number
;		and there is no return address. See *SYS.








;;;
;;; BEGIN
;;;

; TIL pg 136-137 (151-152)

; Class:	Compiler Directive (Immediate)
; Function:	Pushes to the stack the address of the next available free
;		dictionary location.
; Input/Output:	None/One stack entry.
; Usage:	Initiates a BEGIN . . . END loop in the compile mode.
; Code:

	db	5,"BEG"
	__ilink__
begin:	dw	_s_colon
	dw	here	; get @ DP
	dw	s_semi

; Bytes:	12
; Notes:	The immediate form of HERE.








;;;
;;; BINARY
;;;

; TIL pg 137 (152)

; Class:	System
; Function:	Sets the system number base to 2 decimal or the binary
;		radix.
; Input/Output:	None/None.
; Usage:	Sets I/O to the binary radix notation.
; Z80 Code:

	db	6,"BIN"
	__link__
binary:	dw	$+2
	ld	a,2	; get 2 decimal
	ld	(s_base),a	; set BASE to 2
	nxt

; Bytes:	15







if cpm2
;;;
;;; BYE
;;;

	db	3,"BYE"
	__link__
bye:	dw	0
endif







;;;
;;; C!
;;;

; TIL pg 137 (152)

	db	2,"C! "
	__link__
cstore:	dw	$+2
	pop	hl	; get address
	pop	de	; get byte
	ld	(hl),e	; store byte
	nxt








;;;
;;; C+!
;;;

; TIL pg 137-138 (152-153)

; Class:	Memory Reference
; Function:	Pops two stack entries and adds the byte in the low-order
;		byte of the second stack entry to the byte whose address is
;		the top stack entry.
; Input/Output:	Two stack entries/None.
; Usage:	Incrementing/decrementing byte-length data stored in pro¬
;		grammable memory.
; Z80 Code:

	db	3,"C+!"
	__link__
cplusstore:
	dw	$+2
	pop	hl	; get address
	pop	de	; get byte
	ld	a,(hl)	; get at address
	add	e	; add byte
	ld	(hl),a	; store at address
	nxt

; Bytes:	15
; Notes:	No tests for overflow or carry are made.








;;;
;;; C+LOOP
;;;

; TIL pg 138 (153)

; Class:	Compiler Directive (Immediate)
; Function:	Adds the word address of the program control directive
;		*C + LOOP to the dictionary, then computes the difference
;		between the current free dictionary address and the ad¬
;		dress at the top of the stack and encloses the low-order
;		byte in the dictionary as the relative jump byte.
; Input/Output:	One stack entry/None.
; Usage:	Used to terminate a CDO . . . C -I- LOOP construct in the
;		compile mode.
; Code:

	db	6,"C+L"
	__ilink__
cplusloop:
	dw	_s_colon
	dw	literal	; word address of *C+LOOP
	dw	s_cploop
	dw	endcomma	; enclose relative jump byte
	dw	s_semi

; Bytes:	16
; Formal Definition:
;		: BC+LOOP ' *C+LOOP END, ; IMMEDIATE








;;;
;;; C,
;;;

; TIL pg 138 (153)

; Class:		System
; Function:	Pops the top stack word and encloses the low-order byte in
;		the dictionary
; Input/Output	One stack entry/None.
; Usage:		Used to build dictionary keywords.
; Z80 Code:

	db	2,"C, "
	__link__
ccomma:	dw	$+2
	pop	de		; get byte
	ld	hl,(s_dp)		; get @DP
	ld	(hl),e		; store byte
	inc	hl		; bump @dp
	ld	(s_dp),hl		; update @dp
	nxt

; Bytes:		19








;;;
;;; C0SET
;;;

; TIL pg 139 (154)

; Class:	Memory Reference
; Function:	Pops the top stack entry and sets the byte whose address
;		was the top entry to 0.
; Input/Output:	One stack entry/None.
; Usage:	Initializing byte-length data in programmable memory to 0
;		or setting byte-length flags in programmable memory to
;		False.
; Z80 Code:

	db	5,"C0S"
	__link__
c0set:	dw	$+2
	pop	hl	; get address
	ld	(hl),0	; zero @address
	nxt

; Bytes:		13








;;;
;;; C1SET
;;;

; TIL pg 139 (154)

; Class:	Memory Reference
; Function:	Pops the top stack entry and sets the byte whose address
;		was the top entry to 1.
; Input/Output:	One stack entry/None.
; Usage:	Initializing byte-length data in programmable memory to 1
;		or setting byte-length flags in programmable memory to
;		True.
; Z80 Code:

	db	5,"C1S"
	__link__
c1set:	dw	$+2
	pop	hl	; get address
	ld	(hl),1	; one set @address
	nxt

; Bytes:		13








;;;
;;; C<R
;;;

; TIL pg 139 (154)

; Class:	Interstack
; Function:	Pops the top stack entry and pushes the low-order byte to
;		the return stack.
; Input/Output:	One stack entry/One return stack byte entry.
; Usage:	Temporary storage of byte data within a definition, or
;		direct return stack control.
; Z80 Code:

	db	3,"C<R"
	__link__
ctor:	dw	$+2
	pop	hl		; get top byte
	dec	ix		; push it to the
	ld	(ix+0),l	;    return stack
	nxt

; Bytes:	16
; Notes:	Temporary data stored on the return stack must be re¬
;		moved before the end of a definition to prevent incorrect
;		return.








;;;
;;; C?
;;;

; TIL pg 140 (155)

; Class:	I/O
; Function:	Displays to the operator (using the current system number
;		base) the byte whose address is popped from the stack. The
;		number is always followed by a space.
; Input/Output:	One stack entry/None.
; Usage:	Displaying signed numbers to the operator; generally the
;		contents of byte variables or constants.
; Code:

	db	2,"C? "
	__link__
cq:	dw	_s_colon
	dw	cat	; get the byte
	dw	dot	; display it
	dw	s_semi

; Bytes:	14








; C@
;  TIL pg 140 (155)

; Class:	Memory Reference
; Function:	Replaces the address at the top of the stack with the byte at
;		that address (in sign extended format).
; Input/Output: One stack entry/One stack entry.
; Usage:	Returns byte-length data stored in memory in a format
;		compatible with 16-bit signed arithmetic.
; Z80 Code:

	db	2,"C@ "
	__link__
cat:	dw	$+2
	pop	hl	; get address
	ld	e,(hl)	; get byte at address
	ld	a,e	; get the byte
	rla		; sign to CY
	sbc	a,a	; FF if negative else 00
	ld	d,a	; set sign extension
	push	de	; push sixteen 16 word
	nxt

; Bytes:	17








;;;
;;; CA!
;;;

; TIL pg 140 (155)

; Class:	System
; Function:	Stores the address at the top of the stack in the word ad¬
;		dress location of the latest entry in the CURRENT
;		vocabulary, ie: the top stack entry is the code address of
;		the keyword currently in the process of being defined.
; Input/Output:	One stack entry/None.
; Usage:	Used to define defining keywords.
; Code:

	db	3,"CA!"
	__link__
castore:
	dw	_s_colon
	dw	entry	; address of latest header
	dw	cliteral
	db	6	; literal 6
	dw	plus	; header plus 6 equals word address
	dw	store	; store code address
	dw	s_semi

; Bytes:	19
; Formal Definition:
;		: CA! ENTRY 6 + ! ;








;;;
;;; CCONSTANT
;;;

; TIL pg 141 (156)

; Class:	Defining Word
; Function:	Creates a byte constant keyword dictionary entry whose
;		name is the token following CCONSTANT and whose
;		value equals the low-order byte of the top stack entry.
; Input/Output:	One stack entry/None.
; Usage:	Defining byte-length named constants.
; Z80 Code:

	db	9,"CCO"
	__link__
cconstant:
	dw	_s_colon
	dw	create	; create primitive
	dw	ccomma	; store byte to body
	dw	scode	; replace code address
s_cconstant:
	ld	a,(de)	; get byte in code body
	ld	l,a	; to L
	rla		; sign to CY
	sbc	a,a	; FF if neg else 00
	ld	h,a	; set sign extension
	push	hl	; push 16 bit literal
	nxt

; Bytes:	22
; Formal Definition:
;		: CCONSTANT CREATE C, ;CODE ...
; Notes:	THe "..." is the assembly or maching code.








;;;
;;; CDO
;;;

; TIL pg 141 (156)

; Class:	Compiler Directive (Immediate)
; Function:	Encloses the word address of the program control directive
;		*CDO in the dictionary and then pushes the address of the
;		next free dictionary location to the stack.
; Input/Output:	Used to initiate a CDO...CLOOP or CDO...C + LOOP
;		construct in the compile mode.
; Z80 Code:

	db	3,"CDO"
	__ilink__
cdo:	dw	_s_colon
	dw	literal	; word address of *CDO
	dw	s_cdo
	dw	docomma
	dw	s_semi

; Bytes:	16
; Formal Definition:
;		: CDP ' *CDO DO, ; IMMEDIATE








;;;
;;; CI>
;;;

; TIL pg 141-142 (156-157)

; Class:	Interstack
; Function:	Pushes to the stack the loop index for the innermost byte-
;		length loop which is the top return stack byte.
; Input/Output:	One return stack byte/One stack entry and one return
;		stack byte entry.
; Usage:	Retrieval of the current byte loop index.
; Z80 Code:

	db	3,"CI>"
	__link__
cito:	dw	$+2
	ld	l,(ix+0)	; get return top
	ld	a,l		; get the byte
	rla			; sign to CY
	sbc	a,a		; ff if neg else 00
	ld	h,a		; set sign extension
	push	hl		; push 16-bit index
	nxt

; Bytes:	18
; Notes:	Assumes the byte loop index is at the top of the return
;		stack.








;;;
;;; CJ>
;;;

; TIL pg 142 (157)

; Class:	Interstack
; Function:	Pushes to the stack the loop index for the second innermost
;		byte-length loop.
; Input/Output:	Three return stack byte entries./Three return stack entries
;		and one stack entry.
; Usage:	Retrieval of the next level byte loop index.
; Z80 Code:

	db	3,"CJ>"
	__link__
cjto:	dw	$+2
	ld	l,(ix+2)	; get 2nd index
	ld	a,l		; get the byte
	rla			; sign to CY
	sbc	a,a		; ff if neg else 00
	ld	h,a		; set sign extension
	push	hl		; push 16-bit index
	nxt

; Bytes:	18
; Notes:	Assumes the byte loop parameters are on the return
;		stack








;;;
;;; CJOIN
;;;

; TIL pg 142 (157)

; Class:	Stack
; Function:	Pops the top two stack entries and combines them to a
;		single word by moving the low-order byte of the top entry
;		into the high-order byte of the second entry and pushes the
;		resulting 16-bit word to the stack.
; Input/Output: Two stack entries/One stack entry.
; Usage:	Stack manipulation for multi-byte signed integers.
; Z80 Code:

	db	5,"CJO"
	__link__
cjoin:	dw	$+2
	pop	hl	; get low byte
	pop	de	; get high byte
	ld	d,l	; combine
	push	de	; push result
	nxt

; Bytes:	14








;;;
;;; CK>
;;;


; TIL pg 143 (158)

; Class:	Interstack
; Function:	Pushes to the stack the loop index for the third innermost
;		byte-length loop.
; Input/Output:	Five return stack byte entries./Five return stack entries
;		and one stack entry.
; Usage:	Retrieval of the second next level byte loop index.
; Z80 Code:

	db	3,"CK>"
	__link__
ckto:	dw	$+2
	ld	l,(ix+4)	; get 3rd index
	ld	a,l		; get the byte
	rla			; sign to CY
	sbc	a,a		; ff if neg else 00
	ld	h,a		; set sign extension
	push	hl		; push 16-bit index
	nxt

; Bytes:	18
; Notes:	Assumes the byte loop parameters are on the return
;		stack








;;;
;;; CLEAR
;;;

; TIL pg 143 (158)

; Class:	I/O
; Function:	Clears the CRT display and homes the cursor.
; Input/Output:	None/None.
; Usage:	Control of display formatting.
; Z80 Code:

	db	5,"CLE"
	__link__
clear:	dw	$+2
	ld	a,12		; form feed
	call	_echo
	nxt

; Bytes:	15
; Notes:	Presumes that the display driver recognizes a command to
;		clear the screen and homes the cursor.








;;;
;;; CLEAVE
;;;

; Class:	Compiler Directive (Immediate)
; Function:	Encloses the word address of the program control directive
;		* CLEAVE in the dictionary
; Input/Output:	None/None.
; Usage:	Compiles a command to cause an immediate exit from a
;		byte loop construct at execution time. Used within a condi¬
;		tional branch structure.
; Code:

	db	6,"CLE"
	__ilink__
cleave:	dw	_s_colon
	dw	literal		; word address of *CLEAVE
	dw	s_cleave
	dw	comma		; enclose it in the dictionary
	dw	s_semi

; Bytes:	16
; Formal Definition:
;		: CLEAVE ' *CLEAVE , ;








;;;
;;; CLOOP
;;;

; TIL pg 144 (159)

; Class:	Compiler Directive (Immediate)
; Function:	Adds the word address of the program control directive
;		* CLOOP to the dictionary, then computes the difference
;		between the current free dictionary address and the ad¬
;		dress at the top of the stack and encloses the low-order
;		byte in the dictionary.
; Input/Output:	One stack entry/None.
; Usage:	Used to terminate a CDO . . . CLOOP construct in the
;		compile mode.
; Code:

	db	5,"CLO"
	__ilink__
cloop:	dw	_s_colon
	dw	literal		; word address of *CLOOP
	dw	s_cloop
	dw	endcomma	; enclose relative jump loop
	dw	s_semi

; Bytes:	16
; Formal Definition:
;		; CLOOP ' *CLOOP END, ; IMMEDIATE








;;;
;;; COMPILER
;;;

;  TIL pg 144 (159)

; Class:	System Variable
; Function:	Pushes to the stack the address of the compiler variable
;		which points to the last entry in the COMPILER
;		vocabulary.
; Input/Output:	None/One stack entry.
; Usage:	Used to access the link to the last COMPILER vocabulary
;		entry.
; Code:

;  5 SYS COMPILER

	db	8,"COM"
	__link__
compiler:
	dw	_sys
	db	s_compiler-sys_block








;;;
;;; CONSTANT
;;;

; TIL pg 144-145 (159-160)

; Class:	Defining Word
; Function:	Creates a word-length constant keyword dictionary entry
;		whose name is the token following CONSTANT and
;		whose value equals the top stack entry.
; Input/Output:	One stack entry/None.
; Usage:	Defining word-length named constants.
; Z80 Code:

	db	8,"CON"
	__link__
constant:
	dw	_s_colon
	dw	create	; create primitive
	dw	comma	; store number in body
	dw	scode	; replace code address
s_constant:
	ex	de,hl	; word address to HL
	ld	e,(hl)	; get low byte in code body
	inc	hl	; bump pointer
	ld	d,(hl)	; get high byte
	push	de	; number to stack
	nxt

; Bytes:	21
; Formal Definition:
;		: CONSTANT CREATE , ;CODE ...
; Notes:	THe "..." is the assembly or maching code.








;;;
;;; CONTEXT
;;;

;   TIL pg 145 (160)

; Class:	System Variable
; Function:	Pushes to the stack the address of the system context
;		variable.
; Input/Output:	None/One stack entry.
; Usage:	Used to access the system variable which contains the
; 		adddress of the vocabulary that will be searched to locate
;		keywords.
; Code:		Not applicable.

	db	7,"CON"
	__link__
context:
	dw	_sys
	db	s_context-sys_block

; Notes:	In the SYS user block. The code body contains an offset
; 		number and there is no return address.








;;;
;;; CORE
;;;

; TIL pg 145 (160)

; Class:	Vocabulary
; Function:	Sets the CONTEXT system variable to the address of the
;		code body of CORE which contains the address of the
;		latest entry in the vocabulary.
; Input/Output:	None/None.
; Usage:	Evokes the CORE vocabulary.
; Code:		Not applicable.
; Bytes:	12
; Notes:	Predefined but exactly as if defined using the
;		VOCABULARY defining keyword.

	db	4,"COR"
	__link__
core:	dw	s_does
	dw	dovoc
vcore:	dw	lastlink








;;;
;;; CR>
;;;

; TIL pg 146 (161)

; Class:	Interstack
; Function:	Pops the byte at the top of the return stack and pushes it to
;		the stack in sign-extended format.
; Input/Output:	One return stack byte entry/One stack entry.
; Usage:	Retrieval of temporary data stored on the return stack to
;		the stack in a format compatible with signed 16-bit
;		arithmetic.
; Z80 Code:

	db	3,"CR>"
	__link__
crfrom:	dw	$+2
	ld	l,(ix+0)	; get top retrurn byte
	inc	ix		; adjust RSP
	ld	h,0		; assume byte positive
	bit	7,l		; test byte sign
	jr	z,crf1		; if zero, positive
	dec	h		; make negative
crf1:	push	hl
	nxt

; Bytes:	23








;;;
;;; CREATE
;;;

; TIL pg 146 (161)

; Class:	Defining Word
; Function:	Creates a dictionary header for a primitive keyword whose
;		 name is the token following CREATE and links it to the
;		 CURRENT vocabulary.
; Input/Output:	None/None.
; Usage:	Used to create all dictionary headers.
; Code:

	db	6,"CRE"
	__link__
create:	dw	_s_colon
	dw	entry	; pointer to latest header
	dw	aspace	; set the separator
	dw	token	; token to dictionary space
	dw	here	; points to the token
	dw	current	; address of current vocabulary
	dw	at	; vocabulary link
	dw	store	; update link to new token
	dw	cliteral	; four identifier characters
	db	4
	dw	dp	; dictionary pointer
	dw	plusstore	; enclose four characters
	dw	comma	; add link address to new header
	dw	here	; word address of new header
	dw	twoplus	; 2 +: points to code body
	dw	comma	; store at word addresS
	dw	s_semi

; Bytes:	39
; Formal Definition:
;		: CREATE ENTRY ASPACE TOKEN HERE CURRENT @ !
;      4 DP  +! , HERE 2+ , ;








;;;
;;; CRET
;;;

; TIL pg 147 (162)

; Class:	I/O
; Function:	Issues a carriage-return line-feed sequence to the display.
; Input/Output:	None.
; Usage:	Display formatting
; Z80 Code:

	db	4,"CRE"
	__link__
cret:	dw	$+2
	call	_crlf		; call cr/lf routine
	nxt

; Bytes:	13








;;;
;;; CSPLIT
;;;

; TIL pg 147 (162)

; Class:	Stack
; Function:	Pops the top stack entry and creates two 16-bit numbers.
;		The high-order byte is moved to the low-order byte of the
;		second entry in sign-extended format. The low-order byte
;		is returned as the top stack entry as a positive 16-bit
;		number.
; Input/Output:	One stack entry/Two stack entries.
; Usage:	Stack manipulation of multi-byte integers.

	db	6,"CSP"
	__link__
csplit:	dw	$+2
	pop	hl		; get 16 bit number
	ld	e,h		; move high byte
	ld	h,0		; make low + 16 bit
	ld	d,h		; assume positive
	bit	7,e		; test sign
	jr	z,csplit1	; if +, it's ok
	dec	d		; else make negative
csplit1:
	push	de		; push signed byte
	push	hl		; push remainder
	nxt

; Bytes:	22








;;;
;;; CURRENT
;;;

;  TIL pg 147 (162)

; Class:		System Variable
; Function:	Pushes to the stack the address of the current vocabulary
;		variable.
; Input/Output:	None/One stack entry.
; Usage:		Used to access the current vocabulary variable which con¬
;		tains the address of the vocabulary where new keywords
;		will be added.
; Code:		Not applicable.

	db	7,"CUR"
	__link__
current:
	dw	_sys
	db	s_current-sys_block

; Bytes:		9
; Notes:		In the SYS user block. The code body is an offset number
;		and there is no return address. See *SYS.








;;;
;;; CVARIABLE
;;;

; TIL pg 148 (163)

; Class:	Defining Word
; Function:	Creates a byte variable keyword dictionary entry whose
;		name is the token following CVARIABLE and whose ini¬
;		tial value is the low-order byte of the entry popped from
;		the stack.
; Input/Output:	One stack entry/None.
; Usage:	Defining byte-length named variables and initializing
;		them
; Z80 Code:

	db	9,"CVA"
	__link__
cvariable:
	dw	_s_colon
	dw	cconstant	; create header and initialize
	dw	scode		; replace code address and exit
s_cvariable:
	push	de		; push word address
	nxt

; Bytes:	15
; Formal Definition:
;		: CVARIABLE CCONSTANT ;CODE ...
; Notes:	The "...." is assembly or machine code








;;;
;;; D*
;;;

; TIL pg 148-149 (163-164)

; Class:	Arithmetic
; Function:	Does a signed multiply of the second stack word by the
;		low-order byte of the top stack entry and replaces both en¬
;		tries by the 24-bit product with the 8 most significant bits
;		sign extended as the second stack entry and the 16 least
;		significant bits as the top stack entry.
; Input/Output:	Two stack entries/Two stack entries.
; Usage:	Signed integer arithmetic.
; Z80 Code:

	db	2,"D* "
	__link__
dmul:	dw	$+2
	exx			; save IR
	pop	bc		; get 8 bit number
	pop	de		; get 16 bit number
	call	_isign		; field input signs
	call	_udm		; multiply 12x16
	ex	af,af'		; retreive sign flag
	jp	p,dmul1	; if +, it's ok
	ld	a,c		; move 8 most signicant
	cpl			; complement
	ld	c,a		; restore
	ex	de,hl		; move 16 least
	ld	hl,0		; get zero
	sbc	hl,de		; negate 16 least
	jr	nz,dmul1	; if not zero, its ok
	inc	c		; else 2's comp most
dmul1:	push	hl		; 16 least to stack
	push	bc		; 8 most to stack
	exx			; restore IR
	nxt

; Bytes:	39
; Notes:	Does not test the top stack entry to insure it is a valid 8-bit
;		number. The 16 least significant bits are an unsigned
;		number on the stack.








;;;
;;; D/MOD
;;;

; TIL pg 149-150 (164-165)

; Class:	Arithmetic
; Function:	Does a signed divide of the 24-bit number in the third (16
;		least significant bits) and second (8 most significant bits)
;		stack entries by the low-order byte of the top stack entry.
;		Replaces these entries with the 16-bit quotient as the sec¬
;		ond stack entry and the positive 8-bit remainder expanded
;		to 16 bits as the top entry.
; Input/Output:	Three stack entries/Two stack entries.
; Usage:	Signed integer arithmetic.
; Z80 Code:

	db	5,"D/M"
	__link__
ddmod:	dw	$+2
	exx		; save IR
	pop	hl	; get 8 bit divisor
	pop	de	; 8 most significant
	pop	bc	; get 16 least
; dividend DEBC
; divisor  HL

	ld	a,h	; divisor sign
	xor	d	; result sign
	ex	af,af'	; save sign flag
	ld	a,l	; get dividend sign
	and	a	; test sign
	jp	p,mov1	; if +, its ok
	neg		; make positive
mov1:	ld	e,a	; store divisor

; divisor D

	ld	h,b	; get 16 least
	ld	l,c	; to hl

; dividend ..HL

	ld	a,e	; get 8 most
	and	a	; test sign
	jp	p,mov2	; if +. its ok
	cpl		; complement high 8
	ld	hl,0	; else get zero
	sbc	hl,bc	; negate log 16
	jp	nz,mov2	; if non-zero, its ok
	inc	a	; else bump high
; XXX CAC TIL is "ld d,a", but _uddiv expects the divisor in E
mov2:	ld	e,a	; move high 8

; dividend .DHL

	call	_uddiv	; divide 24X8
	call	_osign	; justify result
	push	hl	; quotient to stack
	push	bc	; remainder to stack
	exx		; restore IR
	nxt

; Bytes:	48
; Notes:	Does not test the top stack entry to insure it is a valid 8-bit
;		number. No test is made to insure a valid 16-bit quotient.








;;;
;;; DECIMAL
;;;

; TIL pg 150 (165)

; Class:	System
; Function:	Sets the system variable BASE to 10 decimal to evoke
;		decimal I/O.
; Input/Output:	None/None.
; Usage:	Evokes radix 10 I/O.
; Z80 Code:

	db	7,"DEC"
	__link__
decimal:
	dw	$+2
	ld	a,10		; get 10 decimal
	ld	(s_base), a	; store it at base
	nxt

; Bytes:	15








;;;
;;; DEFINITIONS
;;;

; TIL pg 150 (165)

	db	11,"DEF"
	__link__
definitions:
	dw	$+2
	ld	hl,(s_context)	; context vocabulary
	ld	(s_current),hl	; to current
	nxt

; Bytes:	16








;;;
;;; DISPLAY
;;;

; TIL pg 150 (165)


; Class: I/O
; Function:	Outputs to the display the low-order byte of successive top
;		stack entries until a non-ASCII code is output (a character
;		with the high-order bit 1 set).
; Input/Output:	One to N stack entries/None.
; Usage:	Output to display the stack string data.
; Z80 Code:

	db	7,"DIS"
	__link__
display:
	dw	$+2

if 0
_display:
	exx		; save IR
dsloop:	pop	hl	; get top stack word
	ld	a,l	; low byte
	call	_echo	; display it
	and	a	; test code for bit 7
	jp	p,dsloop	; if positive, loop
	exx		; restore IR
	nxt
endif

;;; This version clips the character to seven bits before displaying it.
if 1
_display:
	exx		; save IR
dsloop:	pop	hl	; get top stack word
	ld	a,l	; low byte
; XXX CAC seven bit ascii only
	and	0x7f
	call	_echo	; display it
	ld	a,l	; low byte
	and	a	; test code for bit 7
	jp	p,dsloop	; if positive, loop
	exx		; restore IR
	nxt
endif








;;;
;;; DO
;;;

; TIL pg 151 (166)

; Class:	Compiler Directive (Immediate)
; Function:	Encloses the word address of the program control directive
;		*DO in the dictionary and then pushes to the stack the ad
;		dress of the next free dictionary location.
; Input/Output: None/One stack entry.
; Usage:	Used to initiate a DO...LOOP or DO... -I-LOOP con
;		struct in the compile mode.
; Code:

	db	2,"DO "
	__ilink__
do:	dw	_s_colon
	dw	literal
	dw	s_do
	dw	docomma
	dw	s_semi

; Bytes:	16
; Formal Definition:
;		: DO ' *DO DO, ;








;;;
;;; DO,
;;;

; TIL pg 151 (166)

; Class:	System
; Function:	Stores the program control directive at the top of the stack
;		to the dictionary and returns the address of the next free
;		dictionary location on the stack.
; Input/Output:	One stack entry/One stack entry.
; Usage:	Used to define compiler directive immediate keywords.
; Code:

	db	3,"DO,"
	__link__
docomma:
	dw	_s_colon
	dw	comma	; store directive
	dw	here	; push free address
	dw	s_semi

; Bytes:	14
; Formal Definition:
;		: DO, , HERE ;








;;;
;;; DOES>
;;;

; TIL pg 151-152 (166-167)

; Class:	Program Control Directive
; Function:	Replaces the first word in the code body of the latest entry
;		in the CURRENT vocabulary with the top return stack
;		word and then replaces its code address with the second
;		return stack entry.
; Input/Output:	Two return stack word entries/None.
; Usage:	Used to terminate the compile time code of a high-level
;		defining word definition. Always followed by keywords
;		that constitute the execution time generic code definition
; Z80 Code:

	db	5,"DOE"
	__link__
does:	dw	_s_colon
	dw	rfrom		; get top return address
	dw	entry		; latest header address
	dw	cliteral	; plus 8
	db	8
	dw	plus		; points to code body
	dw	store		; store return to code body
	dw	scode		; replace code address and return
s_does:	dec	ix		; adjust RSP
	ld	(ix+0),b	; IR low byte to return
	dec	ix		; adjust RSP
	ld	(ix+0),c	; IR high byte to return
	ex	de,hl		; WA register to HL
	ld	c,(hl)		; @WA low into IR
	inc	hl		; bump WA
	ld	b,(hl)		; @WA high into IR
	inc	hl		; bump WA
	push	hl		; push pointer
	nxt

; Bytes:	39
; Formal Definition:
;		: DOES> ENTRY 8 + ! ;CODE ...
; Notes:	THe "..." is assembly or machine code








;;;
;;; DP
;;;

;  TIL pg 152 (167)

;  4 SYS DP

	db	2,"DP "
	__link__
dp:	dw	_sys
	db	s_dp-sys_block








;;;
;;; DROP
;;;

;  TIL pp 152,153 (167,168)

; Class:	Stack
; Function:	Pops the top stack entry and discards it.
; Input/Output:	One stack entry/None.
; Usage:	Stack clean up.
; Z80 Code:

	db	4,"DRO"
	__link__
drop:	dw	$+2
	pop	hl	; drop top
	nxt

; Bytes:	11








;;;
;;; DUMP
;;;

; TIL pg 153 (168)

; Class:	I/O
; Function:	Does a memory dump using the second stack entry as the
;		starting address and the top stack entry as the ending ad¬
;		dress. Displays a line consisting of the address, eight
;		numbers, a space, and eight more numbers. Removes both
;		entries.
; Input/Output:	Two stack entries/None.
; Usage:	Examining memory.
; Code:

	db	4,"DUM"
	__link__
dump:	dw	_s_colon
	dw	over		; get loop starting address
	dw	s_do		; initialize do loop
dump1:	dw	cret		; issue cr-lf
	dw	dup		; duplicate line address
	dw	cliteral	; four char line address minimum
	db	4
	dw	dotr		; print line address
	dw	part		; issue first 8
	dw	part		; issue second 8
	dw	wait		; time to stop and wait
	dw	cliteral	; 16 numbers per line
	db	16
	dw	s_plusloop	; loop until done
	db	dump1-$
	dw	drop		; drop address
	dw	s_semi

; Bytes:	37
; Formal Definition:
;		: DUMP OVER DO CRET DUP 4 .R PART PART WAIT 10 +LOOP DROP ;








;;;
;;; DUP
;;;

; TIL pg 153 (168)

; Class:	Stack
; Function:	Duplicates the top stack entry and pushes it to the stack.
; Input/Output:	One stack entry/Two stack entries.
; Usage:	Stack management.
; Z80 Code:

	db	3,"DUP"
	__link__
dup:	dw	$+2
	pop	hl
	push	hl
	push	hl
	nxt








;;;
;;; ECHO
;;;

; TIL pg 154 (169)

; Class:	I/O
; Function:	Pops the top stack entry and outputs the low-order byte to
;		the display.
; Input/Output:	One stack entry/None.
; Usage:	Direct control of the display for formatting
; Z80 Code:

	db	4,"ECH"
	__link__
echo:	dw	$+2
	pop	hl		; get top
	ld	a,l		; get low-order byte
	call	_echo		; display it
	nxt








;;;
;;; ELSE
;;;

; TIL pg 154 (169)

; Class:	Compiler Directive (Immediate)
; Function:	Encloses the word address of the program control directive
;		*ELSE in the dictionary, saves the address of the next free
;		dictionary location on the stack, reserves 1 byte in the dic¬
;		tionary, swaps the top two stack entries, computes the dif¬
;		ference between the top stack entry and the current free
;		dictionary location and encloses the low-order byte in the
;		dictionary as a relative jump byte.
; Input/Output:	One stack entry/One stack entry.
; Usage:	Used to terminate the True code portion in an IF...ELSE
;		...THEN construct in the compile mode
; Code:

	db	4,"ELS"
	__ilink__
else:	dw	_s_colon
	dw	literal		; word address of *ELSE
	dw	s_else
	dw	docomma		; store address and get pointer
	dw	zero		; get zero
	dw	ccomma		; reserve byte
	dw	swap		; swap top two adresses
	dw	then		; execute then code
	dw	s_semi

; Bytes:	24
; Notes:	See definition of THEN
; Formal Definition:
;		: ELSE ' *ELSE DO, O C, SWAP THEN ; IMMEDIATE
; CAC:
; Actually something like:
;		: ELSE ' *ELSE DO, O C, SWAP ' THEN [ , ] ; IMMEDIATE
; The THEN should not de executed in the ELSE definition, as it will
; be as it is immediate. Rather, the word address of THEN needs to be
; compiled in.








;;;
;;; END
;;;

; TIL pg 154-155 (169-170)

; Class:	Compiler Directive (Immediate)
; Function:	Encloses the word address of the program control directive
;		*END in the dictionary, pops the top stack address, com¬
;		putes the difference between this address and address of
;		the current free dictionary location and encloses the low-
;		order byte in dictionary as a relative jump byte.
; Input/Output:	One stack entry/None.
; Usage:	Used to terminate a BEGIN . . . END loop structure in the
;		compile mode.
; Code:

	db	3,"END"
	__ilink__
end:	dw	_s_colon
	dw	literal		; word address of *END
	dw	s_end
	dw	endcomma	; store and compute jump
	dw	s_semi

; Bytes:	16
; Formal Definition:
;		: END ' *END END, ; IMMEDIATE








;;;
;;; END,
;;;

; TIL pg 155 (170)

; Class:	System
; Function:	Encloses the address of the program control directive at the
;		 top of the stack in the dictionary, computes the relative
;		 jump byte using the top stack entry and the current free
;		 dictionary location and encloses the low-order byte in the
;		 dictionary.
; Input/Output:	Two stack entries/None.
; Usage:	Used in defining compiler directive immediate keywords.
; Code:

	db	4,"END"
	__link__
endcomma:
	dw	_s_colon
	dw	comma	; store directive word address
	dw	here	; current free address
	dw	sub	; compute relative offset
	dw	ccomma	; enclose it in dictionary
	dw	s_semi

; Bytes:	18








;;;
;;; ENTRY
;;;

; TIL pg 155 (170)

; Class:	System
; Function:	Pushes to the stack the address of the first header byte of
;		the latest entry in the CURRENT vocabulary.
; Input/Output:	None/One stack entry.
; Usage:	Used to locate the address of the latest vocabulary definition which
;		will become the link address of the next
;		keyword.
; Code:

	db	5,"ENT"
	__link__
entry:	dw	_s_colon
	dw	current		; current address
	dw	at		; vocabulary address
	dw	at		; header address
	dw	s_semi

; Formal definition:
;		: ENTRY CURRENT @ @ ;








;;;
;;; ERASE
;;;

; TIL pg 155-156 (170-171)

; Class:	Utility
; Function:	Fills a region of memory with ASCII spaces. The starting
;		memory address is the second stack entry and the ending
;		memory address is the top entry. Removes both entries.
; Input/Output:	Two stack entries/None.
; Usage:	Clearing string data.
; Code:

	db	5,"ERA"
	__link__
erase:	dw	_s_colon
	dw	oneplus		; bump last address for looping
	dw	swap		; get loop order correct
	dw	s_do		; initialize loop
erase1:	dw	aspace		; get space code
	dw	ito		; index equals memory access
	dw	cstore		; space to memory
	dw	s_loop		; loop until done
	db	erase1-$
	dw	s_semi

; Bytes:	25








;;;
;;; EXECUTE
;;;

; TIL pg 156 (171)

; Class:	System
; Function:	Pops the top stack entry to the word address register and
;		jumps to the inner interpreter RUN routine to cause the ex¬
;		ecution of a keyword.
; Input/Output:	One stack entry/None.
; Usage:	Used by the system for keyword execution and by operator
; Usage:	for defining conditional execution keywords.

	db	7,"EXE"
	__link__
execute:
	dw	$+2
	pop	hl
	jp	_run	; XXX TIL has this as JR, code is organized differently








;;;
;;; FILL
;;;

; TIL pg 156-157 (171-172)

; Class:	Utility
; Function:	Fill a region of memory with a specified byte. The byte is
;		the third stack entry low-order byte. The starting memory
;		address is the second stack entry and the ending memory
;		address is the top entry. Removes all three entries.
; Input/Output:	Three stack entries/None.
; Usage:	Loading memory to some initial value.
; Code:

	db	4,"FIL"
	__link__
fill:	dw	_s_colon
	dw	oneplus		; dump last address for looping
	dw	swap		; get right loop order
	dw	s_do		; initialize loop
fill1:	dw	dup		; duplicate byte
	dw	ito		; get memory address
	dw	cstore		; store byte
	dw	s_loop		; loop until done
	db	fill1-$
	dw	drop		; remove byte from stack
	dw	s_semi

; Bytes:	27
; Formal Definition:
;		: FILL 1+ SWAP DO DIP I> C! LOOP DROP ;








;;;
;;; FORGET
;;;

; TIL pg 157 (172)

; Class:	Vocabulary
; Function:	Searches the current vocabulary for the token following
;		FORGET. If located, the current link address is set to the
;		address of the link in the keyword located and the dic¬
;		tionary pointer is reset to the start of the header of the
;		located keyword. If not located, the token is echo
;		displayed and followed by a "?".
; Input/Output:	None/None.
; Usage:	Used to delete keyword definitions in a spatial sense.
; Code:

	db	6,"FOR"
	__link__
forget:	dw	_s_colon
	dw	current		; get current address
	dw	at		; points to latest entry in current
	dw	context		; get context address
	dw	store		; set to search current
	dw	tick		; search for token (keyword)
	dw	dup		; need word address twice
	dw	cliteral	; word address less 2 points
	db	2
	dw	sub		;   to the link address
	dw	at		; the link address
	dw	current		; get current address
	dw	at		; points to the link
	dw	store		; reset link to token link
	dw	cliteral	; word address less 6 points to the
	db	6
	dw	sub		;   first header byte of the token
	dw	dp		; get free dictionary address
	dw	store		; reset dictionary free location
	dw	s_semi

; Bytes:	44
; Notes:	Caution is advised. It is possible to forget part or all of the
;		context vocabulary. The end result is an unusable language
;		since nothing can be located.
; Formal Definition:
;		: FORGET CURRENT @ CONTEXT ! ’ DUP 2 - @
;		  CURRENT @ ! 6 - DP ! ;








;;;
;;; HERE
;;;

; TIL pp 157-158 (172-173)

; Class:	System
; Function:	Pushes the address of the next free dictionary location to
;		the stack (the address stored at the system variable DP).
; Input/Output:	None/One stack entry.
; Usage:	Used by the system in building dictionary entries and by
;		the operator to determine dictionary space usage.
; Z80 Code:

	db	4,"HER"
	__link__
here:	dw	$+2
	ld	hl,(s_dp)	; get @DP
	push	hl	; free location to stack
	nxt

; Bytes: 14








;;;
;;; HEX
;;;

; TIL pg 158 (173)

; Class:	System
; Function:	Sets the system variable BASE to 16 decimal to evoke
;		decimal I/O.
; Input/Output:	None/None.
; Usage:	Evokes radix 16 I/O.
; Z80 Code:

	db	3,"HEX"
	__link__
hex:	dw	$+2
	ld	a,16		; get 16 decimal
	ld	(s_base), a	; store it at base
	nxt

; Bytes:	15
; Notes:	Base 16 I/O is the base on start-up








;;;
;;; I>
;;;

; TIL pg 158 (173)

; Class:	Interstack
; Function:	Pushes to the stack the loop index for the innermost word-
;		length loop which is the top return stack word.
; Input/Output:	One return stack word/One return stack word and one
;		stack word.
; Usage:	Retrieval of the current word loop index.
; Z80 Code:

	db	2,"I> "
	__link__
ito:	dw	$+2
	ld	l,(ix+0)		; get low index
	ld	h,(ix+1)		; get high byte
	push	hl			; index to stack
	nxt

; Bytes:	17
; Notes:	Presumes nothing else on the return stack except loop in¬
;		dex.








;;;
;;; IF
;;;

; Class:	Compiler Directive (Immediate)
; Function:	Encloses the word address of the program control directive
;		*IF in the dictionary, pushes the address of the next free
;		dictionary location to the stack and reserves 1 byte in the
;		dictionary for a relative jump byte.
; Input/Output:	None/One stack entry.
; Usage:	Used to initiate a conditional branch construct in the compile mode.
; Code:

	db	2,"IF "
	__ilink__
if_:	dw	_s_colon
	dw	literal		; word address of *IF
	dw	s_if
	dw	docomma		; store address and save pointer
	dw	zero		; get a zero
	dw	ccomma		; reserve a byte
	dw	s_semi

; Bytes:	20
; Formal Definition:
;		: IF ' *IF DO, 0 C, ; IMMEDIATE








;;;
;;; IMMEDIATE
;;;

; TIL pg 159 (174)

; Class:	Vocabulary
; Formal Definition:
; Function:	Delinks the latest entry from the current vocabulary and
;		links it to the compiler vocabulary. The previous second
;		entry in the current vocabulary becomes the latest entry.
; Input/Output:	None/None.
; Usage:	Adding keywords to the compiler vocabulary.
; Code:

	db	9,"IMM"
	__link__
immediate:
	dw	_s_colon
	dw	entry		; points to latest current keyword
	dw	dup		; save it for compiler link
	dw	cliteral	; current header plus 4 points to the
	db	4
	dw	plus		;    latest keywords link
	dw	dup		; save as new link address
	dw	at		; get the link
	dw	current		; points to current
	dw	at		; points to vacabulary
	dw	store		; update current to 2nd keyword
	dw	compiler	; compilers address
	dw	at		; points to the last compiler entry
	dw	swap		; address then link
	dw	store		; store link in previous current
	dw	compiler	; compiler address
	dw	store		; previous current top of compiler
	dw	s_semi

; Bytes:	39
; Formal Definition:
;		: IMMEDIATE ENTRY DUP 4 + DUP @ CURRENT @ 1
;		COMPILER @ SWAP ! COMPILER ! ;








;;;
;;; INLINE
;;;

; TIL pg 88 (103)

inline:	dw	$+2		; primitive code address
	push	bc		; save the IR
inline_start:
	call	_crlf		; issue cr/lf
	ld	hl,lbadd	; get start of line buffer
	ld	(s_lbp),hl	; reset LBP
	ld	b,lblen		; set buffer length
inline_clear:
	ld	(hl),' '	; load space to buffer
	inc	hl		; bump buffer pointer
	djnz	inline_clear	; loop to clear buffer
inline_zero:
	ld	l,0		; back to the first buffer position
inline_inkey:
	call	_key		; input a character
	cp	NAK		; line delete? (^U)
	jr	nz,inline_tstbs	; if not, skip ld code
	call	_echo		; else issue line delete
	jp	inline_start	; and start over
inline_tstbs:
	cp	BS		; is it a backspace? (^H)
	jr	nz,inline_tstcr	; if not, skip backspace code
	dec	l		; decrement buffer ptr
	jp	m,inline_zero	; reset to 0 if negative
	ld	(hl),' '	; load space to the buffer
inline_issue:
	call	_echo		; display the character
	jp	inline_inkey	; and return for the next
inline_tstcr:
	cp	CR		; is it a cr?
	jr	z,inline_last1	; if so, go to the exit line
	bit	7,l		; if bit set, at 129th place (end of buffer)
	jr	nz,inine_end	; do buffer end task at 129
inline_saveit:
	ld	(hl),a		; save characters in buffer
	cp	'a'		; is it less than lower case 'A'?
	jr	c,inline_notlc	; if so, skip lower case code
	cp	'z'		; is it more and lower case 'Z'?
	jr	nc,inline_notlc	; if so, skip lower case code
	res	5,(hl)		; make lower case uppercase in buffer
inline_notlc:
	inc	l		; bump pointer
	jr	inline_issue	; go issue character
inine_end:
	dec	l		; back up to 128th place
	ld	c,a		; save the input character
	ld	a,BS	; get backspace character
	call	_echo		; move cursor leff
	ld	a,c		; restore orignal charactger
	jr	inline_saveit	; go put it at the 128th place
inline_last1:
	ld	a,' '		; replace CR by a space
	call	_echo		; and issue it
	pop	bc		; restore IR
	nxt			; return to next inner interpreter routine








;;;
;;; IOR
;;;

; TIL pg 159-160 (174-175)

; Class:	Logical
; Function:	Replaces the top two stack entries by the logical inclusive
;		or of the entries on a bit-by-bit basis.
; Input/Output:	Two stack entries/One stack entry.
; Usage:	Logical operations.

	db	3,"IOR"
	__link__
ior:	dw	$+2
	pop	hl	; get top word
	pop	de	; get next word
	ld	a,l	; move top low byte
	or	e	; or in 2nd low byte
	ld	l,a	; save low or
	ld	a,h	; move top high byte
	or	d	; or in 2nd high byte
	ld	h,a	; save high or
	push	hl	; push resut
	nxt

; Bytes:	19








;;;
;;; J>
;;;

; TIL pg 160 (175)

; Class:	Interstack
; Function:	Pushes to the stack the loop index for the second innermost
;		word-length loop.
; Input/Output:	Three return stack word entries/Three return stack word
;		entries and one stack entry.
; Usage:	Retrieval of the second level word-length loop index.
; Z80 Code:

	db	2,"J> "
	__link__
jto:	dw	$+2
	ld	l,(ix+4)		; get low index
	ld	h,(ix+5)		; get high byte
	push	hl			; index to stack
	nxt

; Bytes:	17
; Notes:	Presumes only word-length loop parameters on the return
;		stack.








;;;
;;; K>
;;;

; TIL pg 160 (175)

; Class:	Interstack
; Function:	Pushes to the stack the loop index for the third innermost
;		word-length loop.
; Input/Output:	Five return stack word entries/Five return stack word
;		entries and one stack entry.
; Usage:	Retrieval of the third level word-length loop index.
; Z80 Code:

	db	2,"K> "
	__link__
kto:	dw	$+2
	ld	l,(ix+8)		; get low index
	ld	h,(ix+9)		; get high byte
	push	hl			; index to stack
	nxt

; Bytes:	17
; Notes:	Presumes only word-length loop parameters on the return
;		stack.








;;;
;;; KEY
;;;

; TIL ph 161 (175)

; Class:	I/O
; Function:	Pushes to the stack in the low-order byte position the next
;		ASCII code entered via the keyboard.
; Input/Output:	None/One stack entry.
; Usage:	Interfaces the keyboard to the system.
; Z80 Code:

	db	3,"KEY"
	__link__
key:	dw	$+2
	call	_key
	ld	l,a
	push	hl
	nxt

; Bytes:	15
; Notes:	Presumes transfer via the A register.








;;;
;;; LBP
;;;

;   TIL pg 161 (176)


; Class:	System Variable
; Function:	Pushes to the stack the address of the line buffer pointer
;		variable.
; Input/Output:	None/One stack entry.
; Usage:	Used to access the line buffer pointer variable which con¬
;		tains the address of the start of the next token in the input
;		line buffer.

;  2 SYS LBP

	db	3,"LBP"
	__link__
lbp:	dw	_sys
	db	s_lbp-sys_block

; Code:		Not applicable.
; Bytes:	9
; Notes:	In the SYS users block. The code body is an offset number
;		and there is no return address. See *SYS.








;;;
;;; LEAVE
;;;

; TIL pg 161 (176)

; Class:	Compiler Directive (Immediate)
; Function:	Encloses the word address of the program control directive
;		* LEAVE in the dictionary.
; Input/Output:	None/None.
; Usage:	Compiles a command to cause an immediate exit from a
;		word-length loop construct at execution time. Used within
;		a conditional branch construct.
; Code:

	db	5,"LEA"
	__ilink__
leave:	dw	_s_colon
	dw	literal		; word address of *LEAVE
	dw	s_leave
	dw	comma		; enclose it
	dw	s_semi

; Bytes:	16
; Formal Definition:
;		: LEAVE ' *LEAVE , ; IMMEDIATE








;;;
;;; LOOP
;;;

; TIL pg 162 (177)

; Class:	Compiler Directive (Immediate)
; Function:	Encloses the word address of the program control directive
;		*LOOP in the dictionary, then pops the stacks and com¬
;		putes the difference between this address and the next free
;		dictionary address and encloses the low-order byte in the
;		dictionary as the relative jump byte.
; Input/Output:	One stack entry/None.
; Usage:	Used to terminate a DO . . . LOOP construct in the com¬
;		pile mode.
; Code:

	db	4,"LOO"
	__ilink__
loop:	dw	_s_colon
	dw	literal		; word address of *LOOP
	dw	s_loop
	dw	endcomma	; store address and jumpp
	dw	s_semi

; Bytes:	16
; Formal Definition:
;		: LOOP ' *LOOP END, ; IMMEDIATE








;;;
;;; LROT
;;;

; TIL pg 162 (177)

; Class:	Stack
; Function:	Rotates the top three stack entries left in an infix cyclic
;		sense (input ABC into B C A with A the final top stack en¬
;		try).
; Input/Output:	Three stack entries/Three stack entries.
; Usage:	Control of stack order
; Z80 Code:

	db	4,"LRO"
	__link__
lrot:	dw	$+2
	pop	de		; get top
	pop	hl		; get 2nd
	ex	(sp),hl		; exchange 3rd and 2nd
	push	de		; push old top
	push	hl		; push old 3rd
	nxt

; Bytes:	15








;;;
;;; MAX
;;;

; TIL pg 162-163 (177-178)

; Class:	Arithmetic
; Function:	Replaces the top two stack entries by the entry with the
;		higher value (signed).
; Input/Output:	Two stack entries/One stack entry.
; Usage:	Signed integer arithmetic tests.
; Z80 Code:

	db	3,"MAX"
	__link__
max:	dw	$+2
	pop	de		; get top
	pop	hl		; get 2nd
	push	hl		; assume 2nd grater
	and	a		; reset carry
	sbc	hl,de		; 2nd - top
	jp	p,max1		; 2nd greater, exit
	pop	hl		; drop 2nd
	push	de		; push top
max1:	nxt

; Bytes:	21








if cpm2
;;;
;;; STAT
;;;

; XXX debugging code

	db	4,"STA"
	__link__
stat:	dw	_s_colon
	dw	cret		; CR/LF

	dw	literal		; dispaly FBASE
	dw	6
	dw	at
	dw	udot
	dw	s_string
	db	7,"FBASE",CR,LF

	dw	literal		; dispaly TOPRAM
	dw	topram
	dw	cat
	dw	udot
	dw	s_string
	db	8,"TOPRAM",CR,LF

;	dw	literal		; display EOM
;	dw	eom
;	dw	at
;	dw	udot
;	dw	s_string
;	db	5,"EOM",CR,LF

	dw	s_semi
endif







;;;
;;; MIN
;;;

; TIL pg 163 (178)

; Class:	Arithmetic
; Function:	Replaces the top two stack entries by the entry with the
;		smaller value (signed).
; Input/Output:	Two stack entries/One stack entry.
; Usage:	Signed integer arithmetic tests.
; Z80 Code:

	db	3,"MIN"
	__link__
min:	dw	$+2
	pop	de		; get top
	pop	hl		; get 2nd
	push	hl		; assume 2nd smaller
	and	a		; reset carry
	sbc	hl,de		; 2nd - top
	jp	m,min1		; 2nd smaller, exit
	pop	hl		; drop 2nd
	push	de		; push top
min1:	nxt

; Bytes:	21









;;;
;;; MINUS
;;;

; TIL pg 163 (178)

; Class:	Arithmetic
; Function:	Replaces the top stack entry with its two's complement.
; Input/Output:	One stack entry/One stack entry.
; Usage:	Signed integer arithmetic.
; Z80 Code:

	db	5,"MIN"
	__link__
minus:	dw	$+2
	ld	hl,0	; get zero
	pop	de	; get number
	and	a	; reset carry
	sbc	hl,de	; 0 - number
	push	hl	; push 2's complement
	nxt

; Bytes:	18








;;;
;;; MOD
;;;

; TIL pg 163-164 (178-179)

; Class:	Arithmetic
; Function:	Does a signed divide of the second stack word by the low-
;		order byte of the top stack entry. Replaces both entries
;		with the 8-bit remainder expanded to 16 bits.
; Input/Output:	Two stack entries/One stack entry.
; Usage:	Signed integer arithmetic.
; Z80 Code:

	db	3,"MOD"
	__link__
mod_:	dw	$+2
	exx		; save IR
	pop	de	; get 8 bit divisor
	pop	bc	; get 16 bit divedend
	call	_isign	; field input signs
	call	_usdiv	; divide 16x8
	push	bc	; push remainder
	exx		; restore IR
	nxt

; Bytes:	21
; Notes:	No test is made to insure a valid 8-bit divisor.








;;;
;;; MODE
;;;

; TIL pg 164 (179)

; Class:	System Variable
; Function:	Pushes to the stack the address of the system mode
;		variable.
; Input/Output:	None/One stack entry.
; Usage:	Used to access the system variable which contains the
;		system execution state.
; Code:		Not applicable.
;  0 SYS STATE

	db	4,"MOD"
	__link__
mode:	dw	_sys
	db	s_mode-sys_block

; Bytes:	9
; Notes:	In the SYS user block. The code body contains an offset
;		number and there is no return address. See *SYS. If MODE
;		contains 0, the execute mode is in effect and if 1, the com¬
;		pile mode is in effect. MODE is a CVARIABLE and must
;		be referenced using keywords for byte-length addressing.








;;;
;;; MODU/
;;;

; TIL pg 164-165 (179-180)

; Class:	Arithmetic
; Function:	Does a signed divide of the second stack entry by the low-
;		order byte of the top stack entry. Replaces both entries
;		with the positive 8-bit remainder expanded to 16 bits as the
;		second stack entry and the 8-bit quotient expanded to 16
;		bits as the top stack entry.
; Input/Output:	Two stack entries/Two stack entries.
; Usage:	Signed integer arithmetic.
; Z80 Code:

	db	5,"MOD"
	__link__
modudiv:
	dw	$+2
	exx		; save IR
	pop	de	; get 8 bit divisior
	pop	bc	; get 16 bit divedend
	call	_isign	; field input signs
	call	_usdiv	; divide 16x8
	call	_osign	; field output sign
	push	bc	; remainder to stack
	push	hl	; qutoient to stack
	exx		; resotre IR
	nxt

; Bytes:	25
; Notes:	Does not test the divisor to insure it is a valid 8-bit
;		number. No test is made to insure a valid 8-bit quotient.








;;;
;;; MOVE
;;;

; TIL pg 165 (180)

; Class:	Utility
; Function:	Move the region of memory specified by the starting ad-
;		dress of the third stack entry and the ending address of the
;		second stack entry to the memory region specified by the
;		starting address of the top stack entry. Removes all three
;		entries.
; Input/Output:	Three stack entries/None.
; Usage:	Used to move memory data.
; Z80 Code:

	db	4,"MOV"
	__link__
move:	dw	$+2
	exx		; save IR
	pop	de	; new starting address
	pop	hl	; old ending address
	pop	bc	; old starting address
	and	a	; reset carry
	sbc	hl,bc	; count - 1
	push	bc	; old starting
	ex	(sp),hl	; save count -1
	pop	bc	; bc = count - 1
	ex	de,hl	; hl = new starting
	push	hl	; save it
	and	a	; reset carry
	sbc	hl,de	; move from top?
	pop	hl	; get it back
	jr	nc,move2	; no, bottom
	ex	de,hl	; hl + old start
	inc	bc	; bc = count
	ldir		; more the block
move1:	exx		; restore IR
	nxt
move2:	add	hl,bc	; new ending address
	ex	de,hl	; old starting address
	add	hl,bc	; old ending address
	inc	bc	; bc + count
	lddr		; move the block
	jr	move1	; jump to return

; Bytes:	40
; Notes:	The memory blocks may be overlapping, but this routine
;		will correctly move them








;;;
;;; NEXT
;;;

; TIL pg 166 (181)

; Class:	Program Control Directive
; Function:	Encloses a jump to the inner interpreter NEXT routine in
;		the dictionary
; Input/Output:	None/None.
; Usage: Used to terminate keywords defined using machine code.
; Code:

	db	4,"NEX"
	__link__
next:	dw	_s_colon
	dw	literal		; "jp (iy)" instruction
	dw	0xE9FD
	dw	comma
	dw	s_semi

; Bytes: 16
; Formal Definition:
;	HEX : NEXT E9FD , ;








;;;
;;; NOT
;;;

; TIL pg 166 (181)

; Class:	Logical
; Function:	Inverts the logic state of the flag at the top of the stack.
; Input/Output:	One stack entry/One stack entry.
; Usage:	Inverting the results of relational test or other flags
; Z80 Code:

	db	3,"NOT"
	__link__
not_:	dw	$+2
	pop	hl	; get the flag
	ld	a,l	; move low byte
	or	h	; or in the high byte
	ld	de,0	; assume false result
	jr	nz,not1	; if non-zero, false
	inc	e	; make true
not1:	push	de	; flag to stack
	nxt








;;;
;;; NUMBER
;;;

;  TIL pp 94-97 (109-112)

; The NUMBER routine is a headerless primitive called by ?NUMBER to con¬
; vert tokens to binary numbers. It is the single most complex routine in the
; design. On entrance there is a token, in extended header form at the free dic¬
; tionary space, a length character followed by that number of ASCII
; characters. NUMBER will convert this token to a binary number if it is a valid
; number and push the number and a True flag to the stack. If NUMBER deter¬
; mines that the token is not a valid number, it pushes only a False flag (zero) to
; the stack.
; The first character in a valid number token may be an ASCII minus sign
; (hexadecimal 2D). With this exception, all token characters are first tested to
; determine that they are in the set hexadecimal 30 thru 39 by subtracting hexa¬
; decimal 30 from the character (remember that hexadecimal 30 is an ASCII 0
; and hexadecimal 39 is an ASCII 9) and testing to see that the result is between 0
; and 9. If the result is negative the character cannot be in the valid number set.
; If the result is more than hexadecimal 9 but less than hexadecimal 11, it is not
; in the valid character set since an ASCII A less hexadecimal 30 is 11 hexa¬
; decimal. If the result is more than hexadecimal 10, an additional hexadecimal 7
; is subtracted which converts ASCII A,B,...F,G,... to 0A, 0B,...,OF, 10,...
; hexadecimals. The procedure to this point simply converts ASCII characters to
; binary numbers. The number is then tested to verify that it is in the set {0 thru
; (BASE —1)}. If all goes well the token is still in the acceptable number token
; set.
; The overall procedure is a sequential conversion process. The result is first
; set to zero. The process then tests to see if a leading minus sign is present. A
; flag is set to indicate this event. As each token character is scanned and con¬
; verted to a number, the results are updated as:
;
;       Result = Result * BASE + Number
;
; When all token characters have been input, the sign flag is tested. If the
; original token had a leading minus sign, the two's complement of the number
; is saved as the result.
; The procedure is depicted in the flowchart of figure 5.9 and a Z80 listing is
; given in listing 5.5.

	db	6,"NUM"
	__link__
number:	dw	$+2
	exx		; saves IR
	ld	hl,(s_dp)	; get pointer to dictionary
	ld	b,(hl)	; get length of token (count)
	inc	hl	; bump pointer
	ld	a,(hl)	; get first character
	cp	'-'	; is it a minus sign?
	ld	a,0	; set sign flag to false
	jr	nz,skpsav	; if positive, skip to flag save
	dec	a	; make sign flag true
	dec	b	; decrease count by 1
	inc	hl	; dump past minus sign
skpsav:	ex	af,af'	; save sign flag in AF'
	ld	de,0	; zero DE
	push	de	; save as flag
	push	de	; save as result
nloop:	ld	a,(hl)	; get next character
	sub	'0'	; subtract number bias
	jr	c,notno	; if cy == 1, not a number (<0)
	cp	10	; less the 10 (a digit)?
	jr	c,numb	; if cy == 1, it's a digit
	cp	17	; if an uppercase letter, it's over 17
	jr	c,notno	; else it's not a number
	sub	7	; subtract additional letters bias
numb:	ld	e,a	; save binary number in E
	ld	a,(s_base)	; get system number base
	dec	a	; valid set is {0,BASE-1}
	cp	e	; is the binary number valid?
	jr	nc,anumb	; yes; it's a valid number
notno:	pop	hl	; pop result, leaving false on
	exx		;   the stack; restore IR
	nxt
anumb:	ex	(sp), hl	; get result and save ptr
	ex	de,hl	; result to DE as multiplicand
	push	bc	; save count
	push	hl	; save new binary number
	ld	bc,0x0800	; get multiply count
	inc	a	; restore base in a reg (multiplier)
	ld	l,c	; zero hl as the product area
	ld	h,c	;
mloop:	add	hl,hl	; shift product and multiplier
; CAC XXX	adc	a	; left 1 bit
	adc	a,a	; left 1 bit
	jr	nc,skpadd	; if CY == 0, skip add
	add	hl,de	; else add multiplicand
skpadd:	djnz	mloop	; loop to complete multiply
	pop	de	; get binary number back
	add	hl,de	; result = product + number
	pop	bc	; restore count
	ex	(sp),hl	; get pointer and save result
	inc	hl	; bump the ptr
	djnz	nloop	; loop for all characters
	pop	de	; get final result
	pop	hl	; the false flag (a zero)
	ex	af,af'	; get sign flag from AF'
	and	a	; is it zero? (also CY == 0)
	jr	z,done	; skip complement if positive
	sbc	hl,de	; complement result
	ex	de,hl	; final result to DE
done:	push	de	; final result to stack
	scf		; make AF true (!= 0)
	push	af	; push true flag
	exx		; restore IR
	nxt








;;;
;;; OCTAL
;;;

; TIL pg 166-167 (181-182)

; Class:	System
; Function:	Sets the system variable BASE to 8 decimal to evoke octal
;		I/O.
; Input/Output:	None/None.
; Usage:	Evokes radix 8 I/O.
; Z80 Code:

	db	5,"OCT"
	__link__
octal:	dw	$+2
	ld	a,8	; get 8 decimal
	ld	(s_base),a	; store it at base
	nxt








;;;
;;; OUTER
;;;

; TIL pg 80 (95)

outer:	dw	_s_colon
outer0:	dw	type
	dw	inline
outer1:	dw	aspace
	dw	token
	dw	qsearch
	dw	s_if
	db	outer3-$
outer2:	dw	qnumber
	dw	s_end
	db	outer1-$
	dw	question
	dw	s_while
	db	outer0-$
outer3:	dw	q_execute
	dw	s_while
	db	outer1-$







;;;
;;; OVER
;;;

; TIL pg 167 (182)

; Class:	Stack
; Function:	Duplicates the second stack entry and pushes it to the top
;		of the stack.
; Input/Output:	Two stack entries/Three stack entries.
; Usage:	Control of stack order.
; Z80 Code:

	db	4,"OVE"
	__link__
over:	dw	$+2
	pop	hl		; get top
	pop	de		; get 2nd
	push	de		; restore 2nd as 3rd
	push	hl		; restore top as 2nd
	push	de		; restore 2nd as top
	nxt

; Bytes: 15








;;;
;;; PAD
;;;

; TIL -- not in TIL; guessing

; Class:	I/O
; Function:	Pops a count from the stack and emits that many spaces.
; Input/Output:	One stack entry/No stack entries.
; Usage:	Screen formatting.
; Code:

; : PAD DUP IF 0 DO SPACE LOOP ELSE DROP THEN ;

	db	3,"PAD"
	__link__
pad:	dw	_s_colon
	dw	dup
	dw	s_if
	db	pad2-$
	dw	zero
	dw	s_do
pad1:	dw	space
	dw	s_loop
	db	pad1-$
	dw	s_else
	db	pad3-$
pad2:	dw	drop
pad3:	dw	s_semi








;;;
;;; PART
;;;

; TIL pg 167 (182)

; Class:	I/O
; Function:	Pops an address from the stack and displays eight numbers
;		to the operator from the 8 bytes following the initial ad¬
;		dress. The address pointer is left on the stack.
; Input/Output:	One stack entry/One stack entry.
; Usage:	Used by DUMP to display memory.
; Code:

	db	4,"PAR"
	__link__
part:	dw	_s_colon
	dw	space		; issue space to display
	dw	cliteral	; loop ending index
	db	8
	dw	zero		; loop starting index
	dw	s_cdo		; initialize loop
part1:	dw	dup		; duplicate pointer
	dw	cat		; get memory byte
	dw	cliteral	; set to display 3 characters
	db	3
	dw	dotr		; display at least 3
	dw	oneplus		; increment memory pointer
	dw	s_cloop		; loop until done
	db	part1-$
	dw	s_semi

; Bytes:	33
; Formal Definition:
;		: PART SPACE 8 0 COD DUO C@ 3 .R 1+ CLOOP ;








;;;
;;; QUESTION
;;;

; TIL pp 97-98 (112-113)

; The QUESTION keyword is a non-structured primitive. It has a single en¬
; trance but may return to the inner interpreter or may exit to
; START/RESTART via $PATCH. The first character in the token at the free
; dictionary space determines which action will occur. If the high-order bit of
; this character is set, the token is a terminator. This implies that all operator re¬
; quested actions are complete and the line buffer is empty. In this event the ad¬
; dress of the OK message is pushed to the stack arid the routine exits to
; NEXT. The outer interpreter will then jump to TYPE to display this message.
; If the token is not a terminator, it must be an unknown token since it could
; not be found in the dictionary or converted to a valid number. In this event, a
; carriage return-line feed is issued to the display and the unknown token is echo
; displayed to the operator. The address of the ? message is then loaded to the
; WA register and the routine exits to $PATCH. The $PATCH routine will
; patch the system if the unknown token was discovered while the compile mode
; was in effect.
; A listing of the Z80 assembly code for this routine is given in listing 5.6. In
; this listing, note that the primitive TYPE is called as an in-line subroutine by
; changing the IY register to force a return to QUESTION.

	db	8,"QUE"
	__link__
question:	dw	$+2
	ld	hl,(s_dp)	; get pointer to dictionary
	inc	hl		; step over token length
	bit	7,(hl)		; if bit set, terminator
	jr	z,_error	; if not set, an error
	ld	de,ok		; put ok message in WA
	push	de		; save ok message
	nxt
_error:	call	_crlf		; issue cr/lf before unknown token
	ld	iy,return	; set iy to return to this routine
	dec	hl		; back-up to token length
	jp	_type_nopop	; go echo unknown token
return:	ld	de,msgq		; ? message address to wa
	jp	_patch		; go patch system before restart








;;;
;;; R>
;;;

; TIL pg 167-168 (182-183)

; Class:	Interstack
; Function:	Pops the word at the top of the return stack and pushes it
;		to the stack.
; Input/Output:	One return stack word entry/One stack entry.
; Usage:	Retrieval of temporary data stored on the return stack or
;		direct control of return stack addresses.
; Z80 Code:

	db	2,"R> "
	__link__
rfrom:	dw	$+2
	ld	l,(ix+0)	; get return low byte
	inc	ix		; adjust RSP
	ld	h,(ix+0)	; get return high byte
	inc	ix		; adjust RSP
	push	hl		; push to stack
	nxt

; Bytes:	21








;;;
;;; RROT
;;;

; TIL pg 168 (183)

; Class:	Stack
; Function:	Rotates the top three stack entries right in an infix cyclic
;		sense (input ABC into C A B with B the final top stack en¬
;		try).
; Input/Output:	Three stack entries/Three stack entries.
; Usage:	Control of stack order
; Z80 Code:

	db	4,"RRO"
	__link__
rrot:	dw	$+2
	pop	hl		; get top
	pop	de		; get 2nd
	ex	(sp),hl		; top to 3rd
	push	hl		; 3rd to 2nd
	push	de		; 2md to top
	nxt

; Bytes:	15








;;;
;;; S*
;;;

; TIL pg 168 (183)

; Class:	Arithmetic
; Function:	Does a signed multiply of the low-order byte of the second
;		stack entry by the low-order byte of the top stack entry
;		and replaces both entries by the 16-bit product.
; Input/Output:	Two stack entries/One stack entry.
; Usage:	Signed integer arithmetic.
; Z80 Code:

	db	2,"S* "
	__link__
smul:	dw	$+2
	exx			; save IR
	pop	bc		; get first 8 bits
	pop	de		; get 2nd 8 bits
	call	_isign		; field input signs
	call	_usmul		; multiply 8x8
	call	_osign		; justify result
	push	hl		; quotient to stack
	exx			; restore IR
	nxt

; Bytes:	24
; Notes:	No test is made to insure that either stack entry is a valid
;		8-bit number.








;;;
;;; SCODE
;;;

; TIL pg 169 (184)

; Class:	Program Control Directive (Headerless)
; Function:	Resets the code address of the latest keyword in the CUR¬
;		RENT vocabulary to the address at the top of the return
;		stack.
; Input/Output:	One return stack word entry/None.
; Usage:	Used by the system to load the generic code address for
;		defining words at execution time and then return to the
;		outer interpreter.
; Code:
;		R> ;GET RETURN ADDRESS
;		CA! ;STORE IT AS CODE ADDRESS
; Bytes:	8
; Formal Definition:
;		: SCODE R> CA! ;

	; headerless
scode:	dw	_s_colon
	dw	rfrom
	dw	castore
	dw	s_semi








;;;
;;; SEARCH
;;;

; TIL pg 92-93 (107-108)

; SEARCH is the routine within $SEARCH which actually searches the
; vocabularies for a given keyword. It has a header since it will be compiled into
; other keywords after a self-generating language is achieved. I generally code
; this routine as a primitive to insure that keywords can be located as rapidly as
; possible.
;
;    SEARCH is called with the address of the first keyword header in the linked
; list to be searched as the top stack entry (ie: the address of the three in the DUP
; header in the example of figure 2.1). The token being searched for is located in
; the free dictionary space in extended header format. The search routine will
; test the length and up to three characters of the keyword name. The first
; detected mismatch causes the next header in the linked list to become the next
; candidate for a match. This procedure will continue until either a match occurs
; or the bottom of the list is reached (a zero link address). If a match occurs, the
; word address of the located keyword is pushed to the stack followed by a False
; flag. If the bottom of the list is reached, a True flag is pushed to the stack.

;   A flow diagram of the SEARCH routine is given in figure 5.8 and a Z80
; assembly code listing is given in listing 5.4.

	db	6,"SEA"
	__link__
search:	dw	$+2
	exx		; saves IR
	pop	hl	; get 1st header address
testit:	push	hl	; save start of header
	ld	de,(s_dp)	; get dictionary ptr
	ld	c,0	; used with b as a false flag
	ld	a,(de)	; get dictionary token length
	cp	(hl)	; same as keyword length
	jr	nz,nxthdr	; if not, go to next header
	cp	4	; is length over 3?
	jr	c,belo4	; if not, skip 3 set code
	ld	a,3	; set length to 3
belo4:	ld	b,a	; save length as count
nxtch:	inc	hl	; bump header ptr
	inc	de	; bump dictionary ptr
	ld	a,(de)	; get next dictionary character
	cp	(hl)	; match keyword character?
	jr	nz,nxthdr	; if not, go to next header
	djnz	nxtch	; else go test the next character
	pop	hl	; start of found header
	ld	de,6	; start of header plus 6
	add	hl,de	; equals word address
	push	hl	; push WA; BC = 0 for flag
	jr	flag	; done and keyword found
nxthdr:	pop	hl	; get start of current header
	ld	de,4	; plus 4 equals link address
	add	hl,de	; to next keyword
	ld	e,(hl)	; get link addrss of the
	inc	hl	;   start of the next
	ld	d,(hl)	;    header
	ex	de,hl	;
	ld	a,h	; test link address for zero
	or	l	; or last keyword
	jr	nz,testit	; in not 0, test next header
	ld	c,1	; flag = 1 if not found
flag:	push	bc	; push flag
	exx		; restore IR
	nxt








;;;
;;; SIGN
;;;
;
;  TIL pp 169 (184)

; Class:	I/O
; Function:	Pushes the ASCII code for a minus sign to the stack (in the
;		low-order byte position) if the top byte on the return stack
;		is twos complement negative.
; Input/Output:	One return stack byte entry/One return stack byte entry
;		and zero or one stack entries.
; Usage:	Adds a leading negative sign to the stack string number if
;		the original binary number was negative. Used in de¬
;		signing formatted displays.
; Z80 Code

	db	4,"SIG"
	__link__
sign:	dw	$+2
	bit	7,(ix+0)	; get return sign byte
	jr	z,out	; if zero +, exit
	ld	l,'-'	;
	push	hl	; minus sign to stack
out:	nxt

; Bytes:		19








;;;
;;; SINGLE
;;;
;
;  TIL pp 169-170 (184-185)

; Class:	System
; Function:	If the top stack entry is a valid 8-bit number (the high
;		order byte is all zeros or all ones), a False flag is pushed to
;		the stack. Otherwise a True flag is pushed to the stack.
; Input/Output:	One stack entry/Two stack entries.
; Usage:		Used to determine storage or display requirements for
;		stack numbers.
; Z80 Code:

	db	6,"SIN"
	__link__
single:	dw	$+2
	pop	hl	; get word
	push	hl	; restore word
	ld	l,h	; if single, 0 or FFFF
	ld	a,h	; get high byte
	and	a	; test it
	jr	z,s_out	; if zero, push false
	inc	hl	; see note below
s_out:	push	hl	; push flag
	nxt

; Bytes:		18
; Notes:		If the top byte is single, the INC HL instruction will yield a
;		False flag since FFFF -1-1 =0 if and only if the original value
;		of H was FF.








;;;
;;; SPACE
;;;

;  TIL pp 170 (185)

; Class:	I/O
; Function:	Echo displays a space to the display.
; Input/Output:	None/None.
; Usage:	Display formatting.
; Z80 Code:

	db	5,"SPA"
	__link__
space:	dw	$+2
	ld	a,' '		; get ascii space code
	call	_echo		; display it
	nxt

; Bytes:	15








;;;
;;; STATE
;;;

; TIL pg 170 (185)

; Class:	System Variable
; Function:	Pushes to the stack the address of the system state variable.
; Input/Output:	None/One stack entry.
; Usage:	Used to access the system variable which contains the com¬
;		piler immediate state.
; Code:		Not applicable.

;  3 SYS STATE

	db	5,"STA"
	__link__
state:	dw	_sys
	db	s_state-sys_block

; Bytes:	9
; Notes:	In the SYS user block. The code body contains an offset
;		number and there is no return address. See *SYS. STATE
;		is 1 set if a compiler immediate keyword is located in the
;		compile mode and is 0 set when the keyword is executed.
;		STATE is a CVARIABLE and must be referenced using
;		keywords for byte-length addressing.








;;;
;;; SWAP
;;;

; TIL pg 171 (186)

; Class:	Stack
; Function:	Interchanges the order of the top two stack entries.
; Input/Output:	Two stack entries/Two stack entries.
; Usage:	Stack data management.
; Z80 Code:

	db	4,"SWA"
	__link__
swap:	dw	$+2
	pop	hl	; get top
	ex	(sp),hl	; top to 2nd
	push	hl	; 2nd to top
	nxt

; Bytes:	13








;;;
;;; THEN
;;;

; TIL pg 171 (186)

; Class:	Compiler Directive (Immediate)
; Function:	Pops the address from the stack, computes the difference
;		between this address and the current free dictionary loca¬
;		tion as the relative jump byte, and stores the byte at the ad¬
;		dress popped from the stack initially.
; Usage:	Used to terminate a branch construct in the compile mode.
; Code:

	db	4,"THE"
	__ilink__
then:	dw	_s_colon
	dw	here	; get free address
	dw	over	; copy jump address over HERE
	dw	sub	; compute byte jump
	dw	swap	; reverse order
	dw	cstore	; store jump byte
	dw	s_semi

; Bytes:	20
; Notes:	Loads a previously reserved byte in the dictionary. Define
;		THEN as a normal keyword, then define ELSE and WHILE
;		as IMMEDIATES, and finally make THEN an IM¬
;		MEDIATE.
; Formal Definition:
;		: HEN HERE OVER - SWAP C! ;








;;;
;;; TOKEN
;;;

; TIL pg 91 (106)

	db	5,"TOK"
	__link__
token:	dw	$+2
	exx			; saves IR
	ld	hl,(s_lbp)	; get pointer to token
	ld	de,(s_dp)	; get pointer to dictionary
	pop	bc		; seperator ic C, B is zero
	ld	a,' '		;
	cp	c		; is seperator a space?
	jr	nz,token_tok	; if not, token start
token_ignlb:
	cp	(hl)		; is it a space
	jr	nz,token_tok	; if not, token start
	inc	l		; bump the pointer
	jr	token_ignlb	; try the next character
token_tok:
	push	hl		; save token start address
token_count:
	inc	b		; increment count
	inc	l		; bump the pointer
	ld	a,(hl)		; get the next cahracter
	cp	c		; is it a seperator?
	jr	z,token_end	; if so, token end
	rla			; bit 7 to carry
	jr	nc,token_count	; if c == 0 not at end
	dec	l		; back up if a terminator
token_end:
	inc	l		; step past seperator
	ld	(s_lbp),hl	; update lbp for next call
	ld	a,b		; move count to a reg
	ld	(de),a		; length to dictionary
	inc	de		; bump dictionary address
	pop	hl		; get token start address
	ld	c,b		; get count to bc
	ld	b,0		;
	ldir			; move token to dictionary
	exx			; restore IR
	nxt








;;;
;;; TYPE
;;;

; TIL pg 171-172 (186-187)

	db	4,"TYP"
	__link__
type:	dw	$+2
_type:	pop	hl	; get string address
_type_nopop:
	ld	e,(hl)	; get length
.loop:	inc	hl	; bump pointer
	ld	a,(hl)	; get character
	call	_echo	; display it
	dec	e	; decrement length
	jr	nz,.loop	; if length != 0, loop
	nxt







if extensions
;;;
;;; U.
;;;

	db	2,"U. "
	__link__
udot:	dw	_s_colon
	dw	sharpbegin
	dw	sharps
	dw	sharpend
	dw	s_semi
endif







;;;
;;; VARIABLE
;;;

; TIL pg 172 (187)

; Class:	Defining Word
; Function:	Creates a word-length variable dictionary keyword entry
;		whose name is the token following VARIABLE and whose
;		initial value is the value popped from the stack.
; Input/Output:	One stack entry/None.
; Usage:	Defining and initializing word-length named variables
; Z80 Code:

	db	8,"VAR"
	__link__
variable:
	dw	_s_colon
	dw	constant	; create header and initialize
	dw	scode		; replace code address and exit
s_variable:
	push	de		; push word address
	nxt

; Bytes:	15
; Formal Definition:
;		: VARIABLE CONSTANT ;CODE ...
; Notes:	The "...." is the assembly or machine code.








;;;
;;; VOCABULARY
;;;

; TIL pg 172 (187)

; Class:	Defining Word
; Function:	Creates a vocabulary keyword dictionary entry whose
;		name is the token following VOCABULARY, with an ini¬
;		tial link to the latest entry in the CURRENT vocabulary,
;		and which, when the vocabulary name is executed, sets the
;		system variable CONTEXT to the link address.
; Input/Output:	None/None.
; Usage:	Defining vocabularies.
; Code:		<BUILDS ;CREATE THE HEADER AND BODY
;		ENTRY   ;GET CONTEXT LINK
;                       ;STORE IN BODY
;               DOES>   ;RESET CODE ADDRESS, BODY AND EXIT
;		CONTEXT ;GET CONTEXT ADDRESS
;		!       ;STORE LINK TO CONTEXT
; Bytes:	22
; Formal Definition:
;		: VOCABULARY <BUILDS ENTRY , DOES> CONTEXT ! ;

	; XXX Must point to the last dictionary entry

	db	10,"VOC"
	__link__
vocab:	dw	_s_colon
	dw	builds
	dw	entry
	dw	comma
	dw	does
dovoc:	dw	context
	dw	store
	dw	s_semi








;;;
;;; WAIT
;;;

; TIL pg 173 (188)

; Class:	Utility
; Function:	If a keyboard entry (any) has been received, a loop is
;		entered waiting for the next keyboard entry.
; Input/Output:	None/None.
; Usage:	Used to hold the display screen fixed to allow inspection.
; Code:		See notes.
; Bytes:	See notes.
; Notes:	A system specific keyword which first reads the keyboard
;		port without an initial rest. If an entry has been received,
;		the keyboard is sampled until the next entry is received.
;		No keyboard return is expected. The routine should not
;		manipulate the cursor but should simply await the next en¬
;		try. Very system specific.

	db	4,"WAI"
	__link__
wait:	dw	$+2
if cpm2
	push	de
	push	hl
	push	bc
;	ld	c,constatus
;	call	bdoss
;       and	a
;	jr	z,wait2
;	ld	c,conin
;	call	bdoss
	ld	c,conraw
	ld	e,0xff	; Return a character without echoing if one is waiting; zero if none is available
	call	bdoss
	and	a
	jr	z,wait2
	call	_key
wait2:	pop	bc
	pop	hl
	pop	de
	nxt
endif

if raw | mg
	in	a,(port0)	; get keyboard state
;	bit	keyrdy,a		; key pressed?
	and	keyrdy
	jr	z,wait1		; no, exit
	in	a,(port1)	; discard the keypress
	call	_key		; get second keypress
wait1:	nxt			; discard the second keypress
endif








;;;
;;; WHILE
;;;

; TIL pg 173 (188)

; Class:	Compiler Directive (Immediate)
; Function:	Encloses the word address of the program control directive
;		‘WHILE in the dictionary. The top two stack entries are
;		swapped, the top stack entry is popped. The offset from
;		this address to the current free dictionary location is
;		enclosed in the dictionary as a relative jump byte. The
;		function also pops the top stack entry, computes the dif¬
;		ference between this address and the current free dic¬
;		tionary location and stores the low-order byte to the ad¬
;		dress popped from the stack as a relative jump byte.
; Input/Output:	Two stack entries/None.
; Usage:	Used to terminate a loop construct containing a *WHILE.

	db	5,"WHI"
	__ilink__
while:	dw	_s_colon
	dw	swap		; change stack order
	dw	literal		; word address of *WHILE
	dw	s_while
	dw	endcomma	; store address and offset for begin
	dw	then		; store offset for if or else
	dw	s_semi

; Bytes:	20
; Formal Definitaion:
;		: WHILE SWAP 1 *WHILE END, ['] THEN ; IMMEDIATE








;;;
;;; XOR
;;;

; TIL pg 173-174 (188-189)

; Class:	Logical
; Function:	Replaces the top two stack entries by the logical exclusive
;		or of the entries on a bit-by-bit basis.
; Input/Output:	Two stack entries/One stack entry.
; Usage:	Logical operations.
; Z80 Code:

	db	3,"XOR"
	__link__
xor:	dw	$+2
	pop	hl		; get top word
	pop	de		; get 2nd word
	ld	a,l		; move top low byte
	xor	e		; xot in 2nd low byte
	ld	l,a		; move to result
	ld	a,h		; get top high byte
	xor	d		; xor in 2nd high byte
	ld	h,a		; move to result
	push	hl		; result to stack
	nxt

; Bytes:	19








;;;
;;; [
;;;

; TIL pg 174 (189)

; Class:	Compiler Directive (Immediate)
; Function:	Encloses the literal handler *[ in the dictionary, changes
;		the token separator to ] and scans the next token from the
;		input buffer and then encloses the token in extended
;		header format in the dictionary.
; Input/Output:	None/None.
; Usage:	Compiling literal strings into secondary keywords or
;		display formatting.
; Code:

	db	1,"[  "
	__ilink__
string:
	dw	_s_colon
	dw	literal		; word address of *[
	dw	s_string
	dw	comma		; enclose it in the dictionary
	dw	cliteral	; get the seperator ']'
	db	']'
	dw	token		; move token to the dictionary
	dw	here		; get start of token
	dw	cat		; token length
	dw	oneplus		; address of length of token
	dw	dp		; dictionary address
	dw	plusstore	; enclose token in dictionary
	dw	s_semi

; Bytes:	31
; Formal Definition:
;		; [ ' *[ , 5d TOKEN HERE C@ 1+ DP +1 ; IMMEDIATE



if biosdisk

msize:	equ	64	; memsize in kbytes
bias:	equ	(msize-20)*1024	; offset from 20k systen
ccp:	equ	0x3400+bias	; base of the ccp
bios:	equ	ccp+0x1600	; base of the bios	

seldsk:	equ	bios+0x1b
settrk:	equ	bios+0x1e
setsec:	equ	bios+0x21
setdma:	equ	bios+0x24
read:	equ	bios+0x27
write:	equ	bios+0x2a


; TIL pp 210-214


;;;
;;; OPER
;;;

; The operation (0 = Write, 1 = Read)

	db	4,"OPE"
	__link__
oper:	dw	s_cvariable
_oper:	db	0

;;;
;;; TARGET
;;;

; The starting address of the block buffer

	db	6,"TAR"
	__link__
target:	dw	s_variable
_target:
	dw	0

;;;
;;; DRIVE
;;;

; The drive number 0-15

	db	5,"DRI"
	__link__
drive:	dw	s_cvariable
_drive:	db	0

;;;
;;; TRACK
;;;

; The track number

	db	5,"TRA"
	__link__
track:	dw	s_variable
_track:	dw	0

;;;
;;; SECTOR
;;;

; The sector number 1-N

	db	6,"SEC"
	__link__
sector:	dw	s_cvariable
_sector:
	db	0

;;;
;;; S/T
;;;

	db	3,"S/T"
	__link__
spert:	dw	s_cvariable
_spert:	db	0

;;;
;;; BLK2CHS
;;;

	db	7,"BLK"
	__link__
blk2chs:
	dw	_s_colon
	dw	onesub	; convert to 0:n-1
	dw	spert	; sectors/track
	dw	divmod	; /MOD
	dw	oneplus ; sector numbers are 1-N
	dw	sector
	dw	cstore
	dw	track
	dw	store
	dw	s_semi

;;;
;;; GETDSK
;;;

	db	6,"GET"
	__link__
getdsk:	dw	$+2
	push	de
	push	hl
	push	bc

	ld	c,0x0e		; select disk
	ld	a,(_drive)	; 0-15
	ld	e,a
	call	bdoss

	ld	c,0x1f	; get address of DBP to HL
	call	bdoss
	ld	a,(hl)	; get low byte of SPT (sectors per track)
			; if the are more then 127, then we're 
			; in trouble because the /MOD does a
			; signed 8 bit divide
	ld	(_spert),a
	pop	bc
	pop	hl
	pop	de
	nxt

;;;
;;; SETUP
;;;

; drive_number block_number buffer_address SETUP



	db	5,"SET"
	__link__
setup:	dw	_s_colon
	dw	target		; save buffer address
	dw	store
	dw	swap		; drive_number to top
	dw	drive		; save drive number
	dw	cstore
	dw	getdsk		; set S/T
	dw	blk2chs		; block number to track, sector
	dw	s_semi

;;;
;;; DSKI/O
;;;

; leaves on stack
;  low byte error code
;  high byte: which call
;    1: seldsk
;    2: read
;    3: write

	db	6,"DSK"
	__link__
dskio:	dw	$+2
	push	hl
	push	bc

	ld	a,(_drive)	; 0-15
	ld	c,a
	ld	d,0xff		; existing disk
	ld	e,0xff
	call	seldsk
				; HL is zero on error
	ld	a,h
	or	a
	jr	nz,_diskio_settrk
	ld	a,l
	or
	jr	nz,_diskio_settrk

	; HL is zero; error on seldsk call
	
	ld	d,1		; error was in seldsk
	ld	e,1		; HL returned as zero
	jr	_diskio_ret


_diskio_settrk:
	ld	a,(_track)	; 0-N
	ld	b,a
	ld	c,0
	call	settrk

	ld	a,(_sector)	; 1-N
	ld	b,a
	ld	c,0
	call	setsec

	ld	bc,(_target)
	call	setdma

	ld	a,(_oper)
	and	a
	jr	z,_diskio_write

	call	read
	or	a
	jr	z,_diskio_iodone

	; read error
	ld	d,2		; error was in read
	ld	e,a		; returned error code
	jr	_diskio_ret

_diskio_write:
	ld	c,1	; immediate
	call	write
	or	a
	jr	z,_diskio_iodone

	; write error
	ld	d,3		; error was in write
	ld	e,a		; returned error code
	jr	_diskio_ret


_diskio_iodone:
	ld	e,0
	ld	d,0

_diskio_ret:
	pop	bc
	pop	hl

	push	de	; push BIOS return value
	nxt

;;;
;;; GETIT
;;;

	db	5,"GET"
	__link__
getit:	dw	_s_colon
	dw	setup
	dw	oper
	dw	c1set
	dw	dskio
	dw	s_semi

;;;
;;; PUTIT
;;;

	db	5,"PUT"
	__link__
putit:	dw	_s_colon
	dw	setup
	dw	oper
	dw	c0set
	dw	dskio
	dw	s_semi


endif ; biosdisk

if bdosdisk

; bdosdisk code taken from https://github.com/dimitrit/figforth.git
; under GPL V3 license

; CP/M BDOS CALLS USED (as per Albert van der Horst, HCCH)
;
; R/W reads or writes a sector in the file specified when invoking
; Z80 fig-FORTH (A>Z80FORTH d:filename.ext), using the default FCB.
; More than one disc may be accessed by temporary use of a user de-
; fined FCB.
;

; Memory allocation

nscr:	equ	4	; # if 1024 byte screens
kbbuf:	equ	128	; bytes/disc buffer
co:	equ	kbbuf+4	; disc buff + 2 header + 2 tail
nbuf:	equ	nscr*0x400/kbbuf	; number of buffers
bufsiz:	equ	co*nbuf	; total disk buffer size


deffcb:	equ	0x005c	; default FCB
;
; CP/M Functions
;
opnfil:	equ    0x0f             ; open file
clsfil:	equ    0x10             ; close file
setdma:	equ    0x1a             ; set DMA address
wrtrnd:	equ    0x22             ; write random
;
maxlen:	equ    0x08             ; max filename length
ftlen:	equ    0x03             ; filetype length

;;;
;;; BDOS
;;;

	db	4,"BDO"
	__link__
bdos:	dw	$+2
	exx			; save IR
	pop	bc		; (C) <-- (S1(LB = CP/M function code
	pop	de		; (DE) <== (S2)  = parameter
	push	ix
	push	iy
	exx
	push	bc		; save ip
	exx
	call	bdoss
	exx
	pop	bc		; restore ip
	exx
	pop	iy
	pop	ix
	exx			; restore ip
	ld	l,a
	ld	h,0
	push	hl		; (S1) <-- (HL) = returned value
	nxt

;;;
;;; LIMIT
;;;

	db	5,"LIM"
	__link__
limit:	dw	s_constant
_limit:	dw	0		; initialized by CLD

;;;
;;; FIRST
;;;

	db	5,"FIR"
	__link__
first:	dw	s_constant
_first:	dw	0		; initialized by CLD

;;;
;;; PREV
;;;

	db	4,"PRE"
	__link__
prev:	dw	s_variable
_prev:	dw	0		; initialized by CLD

;;;
;;; DISK-ERROR
;;;

	db	10,"DIS"
	__link__
dskerr:	dw	s_variable
	dw	0

;;;
;;; FCB
;;;

; Current FCB address

	db	3,"FCB"
	__link__
fcb:	dw	s_constant
	dw	deffcb


;;;
;;; REC#
;;;

; Returns address of random rec.#

	db	4,"REC"
	__link__
recadr:	dw	_s_colon
	dw	fcb
	dw	cliteral
	db	0x21
	dw	plus
	dw	s_semi

;;;
;;; USE
;;;

	db	3,"USE"
	__link__
use:	dw	s_variable
_use:	dw	0		; initialized by CLD


;;;
;;; #BUF
;;;

	db	4,"#BU"
	__link__
nobuf:	dw	s_constant
	dw	nbuf

;;;
;;; R/W
;;;

;      R/W           addr  blk  f  ---
;               The fig-FORTH standard disc read-write linkage.  addr
;               specifies the source or destination block buffer, blk is
;               the sequential number of the referenced block; and f is a
;               flag for f=0 write and f=1 for read.  R/W determines the
;               location on mass storage, performs the read-write and
;               performs any error checking.

	db	3,"R/W"
	__link__
rslw:	dw	_s_colon
	dw	tor		; store r/w flag
	dw	recadr
	dw	store		; set record number
	dw	zero
	dw	recadr
	dw	twoplus
	dw	cstore

	dw	literal		; set dma addrss
	dw	setdma
	dw	bdos
	dw	drop

	dw	literal
	dw	wrtrnd
	dw	rfrom
	dw	sub		; select read or write
	dw	fcb
	dw	swap
	dw	bdos
	dw	dskerr
	dw	store
	dw	s_semi


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Disc interface high level routines
;;;

;;;
;;; +BUF
;;;

	db	4,"+BUF"
	__link__
pbuf:	dw	_s_colon
	dw	literal
	dw	co
	dw	plus
	dw	dup
	dw	limit
	dw	equals
	dw	s_if
	db	pbuf1-$
	dw	drop
	dw	first
pbuf1:	dw	dup
	dw	prev
	dw	at
	dw	sub
	dw	s_semi

;;;
;;; UPDATE
;;;

	db	6,"UPD"
	__link__
update:	dw	_s_colon
	dw	prev
	dw	at
	dw	at
	dw	literal
	dw	0x8000
	dw	ior
	dw	prev
	dw	at
	dw	store
	dw	s_semi

;;;
;;; EMPTY-BUFFERS
;;;

	db	13,"EMP"
	__link__
mtbuf:	dw	_s_colon
	dw	first
	dw	limit
	dw	over
	dw	sub
	dw	erase
	dw	s_semi

;;;
;;; OFFSET
;;;

	db	6,"OFF"
	__link__
offset:	dw	_sys
	db	s_offset-sys_block

;;;
;;; BLANK
;;;

	db	5,"BLA"
	__link__
blank:	dw	_s_colon
	dw	aspace
	dw	fill
	dw	s_semi

;;;
;;; BUFFER
;;;

	db	6,"BUF"
	__link__
buffer:	dw	_s_colon
	dw	use
	dw	at
	dw	dup
	dw	tor
buffer1:
	dw	pbuf		; won't work if single buffer
	dw	s_if
	db	buffer1-$
	dw	use
	dw	store
	dw	ito		; original is "R"
	dw	at
	dw	zless
	dw	s_if
	db	buffer2-$
	dw	ito		; original is "R"
	dw	twoplus
	dw	ito		; original is "R"
	dw	at
	dw	literal
	dw	0x7fff
	dw	and
	dw	zero
	dw	rslw
buffer2:
	dw	ito		; original is "R"
	dw	store
	dw	ito		; original is "R"
	dw	prev
	dw	store
	dw	rfrom
	dw	twoplus
	dw	s_semi

;;;
;;; BLOCK
;;;

	db	5,"BLO"
	__link__
block:	dw	_s_colon
	dw	offset
	dw	at
	dw	plus
	dw	tor
	dw	prev
	dw	at
	dw	dup
	dw	at
	dw	ito
	dw	sub
	dw	dup
	dw	plus
	dw	s_if
	db	block1-$
block2:	dw	pbuf
	dw	zeq
	dw	s_if
	db	block1-$
	dw	drop
	dw	ito
	dw	buffer
	dw	dup
	dw	ito
	dw	one
	dw	rslw
	dw	twosub
block3:	dw	dup
	dw	at
	dw	ito
	dw	sub
	dw	dup
	dw	plus
	dw	zeq
	dw	s_if
	db	block2-$
	dw	dup
	dw	prev
	dw	store
block1:	dw	rfrom
	dw	drop
	dw	twoplus
	dw	s_semi

;;;
;;; FLUSH
;;;

	db	5,"FLU"
	__link__
flush:	dw	_s_colon
	dw	nobuf
	dw	oneplus
	dw	zero
	dw	s_do
flush1:	dw	zero
	dw	buffer
	dw	drop
	dw	s_loop
	db	flush1-$
	dw	s_semi

;;;
;;; EXTEND
;;;

	db	6,"EXT"
	__link__
extend:	dw	_s_colon
	dw	here		; fill with b/buf blanks
	dw	blank
	dw	cliteral
	db	8
	dw	mul
	dw	zero
extend1:
	dw	oneplus		; begin
	dw	here
	dw	over
	dw	one
	dw	rslw
	dw	dskerr
	dw	at
	dw	s_if
	db	extend1-$
	dw	swap
	dw	over
	dw	plus
	dw	swap
	dw	s_do
extend2:
	dw	here
	dw	ito
	dw	zero
	dw	rslw
	dw	s_loop
	db	extend2-$
	dw	fcb
	dw	cliteral
	db	clsfil
	dw	bdos
	dw	drop
	dw	fopen
	dw	drop
	dw	s_semi

;;;
;;; FOPEN ( -- f )
;;;  Opens a file that currently exists in the
;;;  disk directory for the currently active
;;;  user number. A true flag indicates failure
;;;

	db	5,"FOP"
	__link__
fopen:	dw	_s_colon
	dw	fcb
	dw	cliteral
	db	opnfil		; open file
	dw	bdos
	dw	literal		; check for error
	dw	0xff		
	dw	equals
	dw	dup
	dw	zeq
	dw	warn
	dw	store
	dw	s_semi

;;;
;;; FTYPE ( --- addr )
;;;

	db	5,"FTY"
	__link__
ftype:	dw	s_constant
	dw	defft
defft:	db	"FTH"

;;;
;;; COUNT
;;;

	db	5,"COU"
	__link__
count:	dw	_s_colon
	dw	dup
	dw	oneplus
	dw	swap
	dw	cat
	dw	s_semi

;;;
;;; WARN
;;;

;;;
;;; ERROR
;;;

	db	5,"ERR"
	__link__
error:	dw	_s_colon
	dw	warn
	dw	at
	dw	zless
	dw	s_if
	db	error1-$
	dw	s_abort
error1:	dw	here
	dw	count
	dw	type
	dw	pdotq
;;;
;;; QERROR
;;;

	db	6,"?ER"
	__link__
qerror:	dw	_s_colon
	dw	swap
	dw	s_if
	db	qerror1-$
	dw	error
	dw	s_else
	db	qerror2-$
qerror1:
	dw	drop
qerror2:
	dw	s_semi


;;;
;;; FILEOPEN  --  FILEOPEN cccc
;;;

	db	8,"FIL"
	__link__
file:	dw	_s_colon
	dw	fcb
	dw	cliteral
	db	clsfil		; close existing file
	dw	bdos
	dw	drop
	dw	mtbuf		; clear buffer
	dw	fcb		; clear FCB
	dw	cliteral
	db	0x10
	dw	zero
	dw	fill
	dw	aspace
	dw	token		; get filename
	dw	here
	dw	count
	dw	cliteral
	db	maxlen
	dw	min		; truncate file name if needed
	dw	fcb
	dw	oneplus
	dw	dup
	dw	cliteral
	db	maxlen
	dw	blank		; blank filename if fcb
	dw	ftype
	dw	over
	dw	cliteral
	db	maxlen
	dw	plus
	dw	literal
	dw	ftlen
	dw	move		; set file type
	dw	swap
	dw	move
	dw	fopen
	dw	cliteral
	db	8
	dw	qerror
	dw	s_semi

endif ; bdosdisk



;;;
;;; End of dictionary; must be last!
;;;

dp0:

lastilink	equ	__ilink
lastlink	equ	__link

	end

