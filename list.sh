#! /usr/bin/awk -f 

# Select one of on the commandline:
#
#   -v raw=1
#   -v mg=1
#   -v cpm2=1
#
# raw:  build for altairz80, no ROM
# mg:   build for MartyG's system
# cpm2: build for CP/M V2 (uses BDOS calls for I/O)
# 
# For CP/M BIOS Disk I/O
#
#  -v biosdisk=1
#
# For CP/M BDOS Disk I/O
#
#  -v bdosdisk=1
#
# Debugging extenstions
#
#  -v ext=1




BEGIN { keep = 1; n = 0; prv = "0004"; ni = 0; prvi = "0004"; 
}

# Doesn't handle nested if

# catch conditionals

/^else$/ { keep = 1 - keep }
/^endif/ { keep = 1 }
/^if cpm2 == 1/ { keep = cpm2 }
/^if cpm2 == 0/ { keep = 1 - cpm2 }
/^if mg == 1/ { keep = mg }
/^if mg == 0/ { keep = 1 - mg }
/^if raw == 1/ { keep = raw }
/^if raw == 0/ { keep = 1 - raw }
/^if biosdisk == 1/ { keep = biosdisk }
/^if biosdisk == 0/ { keep = 1 - biosdisk }
/^if bdosdisk == 1/ { keep = bdosdisk }
/^if bdosdisk == 0/ { keep = 1 - bdosdisk }
/^if ext == 1/ { keep = ext }
/^if ext == 0/ { keep = 1 - ext }
/^if 0/ { keep = 0 }
/^if 1/ { keep = 1 }


# dictionary link list

/^\t__link__/ { if (keep) { nxt = sprintf ("L_%04d", n); printf ("%s:\tdw\t%s-4\n", nxt, prv); prv = nxt; n = n + 1; next  }}
/^\t__ilink__/ { if (keep) { nxt = sprintf ("I_%04d", ni); printf ("%s:\tdw\t%s-4\n", nxt, prvi); prvi = nxt; ni = ni + 1; next  }}
/^\t__lastlink__/ { printf ("lastlink:\tequ\t%s-4\n", prv); next }
/^\t__lastilink__/ { printf ("lastilink:\tequ\t%s-4\n", prvi); next }


# conditional build equates

/^\t__raw__/ { printf ("raw:\tequ\t%d\n", raw == 1 ? 1 : 0); next }
/^\t__mg__/ { printf ("mg:\tequ\t%d\n", mg == 1 ? 1 : 0); next }
/^\t__cpm2__/ { printf ("cpm2:\tequ\t%d\n", cpm2== 1 ? 1 : 0); next }
/^\t__biosdisk__/ { printf ("biosdisk:\tequ\t%d\n", biosdisk== 1 ? 1 : 0); next }
/^\t__bdosdisk__/ { printf ("bdosdisk:\tequ\t%d\n", bdosdisk== 1 ? 1 : 0); next }
/^\t__ext__/ { printf ("ext:\tequ\t%d\n", ext== 1 ? 1 : 0); next }


{ print $0 }
