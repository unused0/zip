all : dictionary_check mg.bin zip.bin trsdos.hex # cpm2.hex

.PHONY : dictionary_check

dictionary_check : zip.asm
	@echo "Duplicate entry check:"
	@if (awk -F '\t' /'^[a-zA-Z0-9_:]*\tdb\t[0-9]+,"..."/ { print $$3 }'  zip.asm  | sort -n | uniq -c | grep -v "      1 ") ; then \
	  echo ; \
	else \
	  echo "no dups";\
	fi


zip.lst zip.bin : zip.asm
	zmac -c -j -L -m --oo cim,lst -o zip.cim -o zip.lst -z -Draw --od . zip.asm
	mv zip.cim zip.bin 

mg.bin : zip.asm
	zmac -c -j -L -m --oo cim,lst -o mg.cim -o mg.lst -z -Dmg --od . zip.asm
	mv mg.cim mg.bin 

cpm2.hex : zip.asm
	zmac -c -j -L -m --oo hex,lst -o cpm2.hex -o cpm2.lst -z -Dcpm2 --od . zip.asm

trsdos.hex : zip.asm
	zmac -c -j -L -m --oo hex,lst -o trsdos.hex -o trsdos.lst -z -Dtrsdos --od . zip.asm

test :	zip.bin
	./altairz80 zipd.ini
	./annotate.sh zip < zip.debug > zip.debug.annotate


check:	zip.bin zip.lst
	expect check.expect `awk < zip.lst '/^_error / { print substr ("0000", 1, 4 - length ($$2)) $$2 }'` `awk < zip.lst '/^sys_block / { print substr ("0000", 1, 4 - length ($$2)) $$2 }'`

coverage: zip.bin coverage_check zip.lst
	expect check.expect `awk < zip.lst '/^_error / { print substr ("0000", 1, 4 - length ($$2)) $$2 }'` `awk < zip.lst '/^sys_block / { print substr ("0000", 1, 4 - length ($$2)) $$2 }'`
	./coverage_check > coverage.list
	grep < coverage.list "^     ....:.[[:xdigit:]][[:xdigit:]][[:xdigit:]][[:xdigit:]]  [[:xdigit:]]"

coverage_check : coverage_check.c

clean :
	-rm coverage.dat coverage.list coverage_check mg.lst mg.bin zip.lst zip.hex cpm2.lst cpm2.hex trsdos.lst trsdos.hex zip.debug zip.annotate 
